module Karcero.Core.Tests.Actors

open Expecto
open Karcero.Core
open Movement
open Actors

[<Tests>]
let ActorsTests =
    testList
        "Actors Tests"
        [ testCase "can determine one action movement for a character."
          <| fun () ->
              let movement = start |> add Straight Normal

              let actual = newPc |> actionCostForMovement 0<ft> 0 movement Speed

              isEqual actual 1<actions>

          testCase "can determine two action movement for a character."
          <| fun () ->
              let movement = start |> add Straight Normal

              let actual = newPc |> actionCostForMovement 25<ft> 0 movement Speed

              isEqual actual 2<actions>

          testCase "can determine three action movement for a character."
          <| fun () ->
              let movement = start |> add Straight Normal

              let actual = newPc |> actionCostForMovement 50<ft> 0 movement Speed

              isEqual actual 3<actions>

          testCase "can determine character size for tiny tall creatures."
          <| fun () ->
              let actual =
                  { newPc with Size = { Category = Tiny ; Orientation = Tall } } |> characterReach

              isEqual actual 0<ft>

          testCase "can determine character size for tiny long creatures."
          <| fun () ->
              let actual =
                  { newPc with Size = { Category = Tiny ; Orientation = Long } } |> characterReach

              isEqual actual 0<ft>

          testCase "can determine character size for small tall creatures."
          <| fun () ->
              let actual =
                  { newPc with
                      Size =
                          { Category = Small
                            Orientation = Tall } }
                  |> characterReach

              isEqual actual 5<ft>

          testCase "can determine character size for small long creatures."
          <| fun () ->
              let actual =
                  { newPc with
                      Size =
                          { Category = Small
                            Orientation = Long } }
                  |> characterReach

              isEqual actual 5<ft>

          testCase "can determine character size for medium tall creatures."
          <| fun () ->
              let actual =
                  { newPc with
                      Size =
                          { Category = CharacterSizeCategory.Medium
                            Orientation = Tall } }
                  |> characterReach

              isEqual actual 5<ft>

          testCase "can determine character size for medium long creatures."
          <| fun () ->
              let actual =
                  { newPc with
                      Size =
                          { Category = CharacterSizeCategory.Medium
                            Orientation = Long } }
                  |> characterReach

              isEqual actual 5<ft>

          testCase "can determine character size for large tall creatures."
          <| fun () ->
              let actual =
                  { newPc with
                      Size =
                          { Category = Large
                            Orientation = Tall } }
                  |> characterReach

              isEqual actual 10<ft>

          testCase "can determine character size for large long creatures."
          <| fun () ->
              let actual =
                  { newPc with
                      Size =
                          { Category = Large
                            Orientation = Long } }
                  |> characterReach

              isEqual actual 5<ft>

          testCase "can determine character size for huge tall creatures."
          <| fun () ->
              let actual =
                  { newPc with Size = { Category = Huge ; Orientation = Tall } } |> characterReach

              isEqual actual 15<ft>

          testCase "can determine character size for huge long creatures."
          <| fun () ->
              let actual =
                  { newPc with Size = { Category = Huge ; Orientation = Long } } |> characterReach

              isEqual actual 10<ft>

          testCase "can determine character size for gargantuan tall creatures."
          <| fun () ->
              let actual =
                  { newPc with
                      Size =
                          { Category = Gargantuan
                            Orientation = Tall } }
                  |> characterReach

              isEqual actual 20<ft>

          testCase "can determine character size for gargantuan long creatures."
          <| fun () ->
              let actual =
                  { newPc with
                      Size =
                          { Category = Gargantuan
                            Orientation = Long } }
                  |> characterReach

              isEqual actual 15<ft>

          testCase "can determine space for tiny characters"
          <| fun () ->
              let actual =
                  { newPc with Size = { newPc.Size with Category = Tiny } } |> characterSpace

              isEqual actual 0<ft>

          testCase "can determine space for small characters"
          <| fun () ->
              let actual =
                  { newPc with Size = { newPc.Size with Category = Small } } |> characterSpace

              isEqual actual 5<ft>

          testCase "can determine space for medium characters"
          <| fun () ->
              let actual =
                  { newPc with Size = { newPc.Size with Category = CharacterSizeCategory.Medium } }
                  |> characterSpace

              isEqual actual 5<ft>

          testCase "can determine space for large characters"
          <| fun () ->
              let actual =
                  { newPc with Size = { newPc.Size with Category = Large } } |> characterSpace

              isEqual actual 10<ft>

          testCase "can determine space for huge characters"
          <| fun () ->
              let actual =
                  { newPc with Size = { newPc.Size with Category = Huge } } |> characterSpace

              isEqual actual 15<ft>

          testCase "can determine space for gargantuan characters"
          <| fun () ->
              let actual =
                  { newPc with Size = { newPc.Size with Category = Gargantuan } }
                  |> characterSpace

              isEqual actual 20<ft>

          testCase "can determine AC for a player character"
          <| fun () ->
              let armor =
                  { Id = "my-cool-armor"
                    Name = "Cool Armor"
                    Description = ""
                    Hardness = 0
                    MaterialHp = 0
                    BreakingThreshold = None
                    Category = Heavy
                    Level = 0
                    Rarity = Common
                    Traits = []
                    NormalBulk = Bulk 2
                    EquippedBulk = Bulk 2
                    AcBonus = 3
                    DexterityModCap = 2
                    StrengthRequirement = Some 14
                    CheckPenalty = None
                    SpeedPenalty = Some -5<ft>
                    Price = Some (gp 2) }

              let actual =
                  { newPc with
                      Level = 1
                      Dexterity = 10
                      Strength = 14
                      Armor = Some armor
                      ArmorProficiencies = newPc.ArmorProficiencies |> Map.add Heavy Trained }
                  |> calculatePcArmorClass

              isEqual actual 16

          testCase "cannot equip armor if strength requirement not met"
          <| fun () ->
              let armor =
                  { Id = "my-cool-armor"
                    Name = "Cool Armor"
                    Description = ""
                    Hardness = 0
                    MaterialHp = 0
                    BreakingThreshold = None
                    Category = Heavy
                    Level = 0
                    Rarity = Common
                    Traits = []
                    NormalBulk = Bulk 2
                    EquippedBulk = Bulk 2
                    AcBonus = 3
                    DexterityModCap = 2
                    StrengthRequirement = Some 14
                    CheckPenalty = None
                    SpeedPenalty = Some -5<ft>
                    Price = Some (gp 2) }

              let expected : Result<PlayerCharacter, KarceroError> =
                  Error (ArmorEquipError StrTooLow)

              let actual =
                  { newPc with
                      Strength = 12
                      ArmorProficiencies = newPc.ArmorProficiencies |> Map.add Heavy Trained }
                  |> pcEquipArmor armor

              isEqual actual expected

          testCase "cannot equip armor if proficiency too low"
          <| fun () ->
              let armor =
                  { Id = "my-cool-armor"
                    Name = "Cool Armor"
                    Description = ""
                    Hardness = 0
                    MaterialHp = 0
                    BreakingThreshold = None
                    Category = Heavy
                    Level = 0
                    Rarity = Common
                    Traits = []
                    NormalBulk = Bulk 2
                    EquippedBulk = Bulk 2
                    AcBonus = 3
                    DexterityModCap = 2
                    StrengthRequirement = Some 14
                    CheckPenalty = None
                    SpeedPenalty = Some -5<ft>
                    Price = Some (gp 2) }

              let expected : Result<PlayerCharacter, KarceroError> =
                  Error (ArmorEquipError ProfTooLow)

              let actual =
                  { newPc with
                      Strength = 15
                      ArmorProficiencies = newPc.ArmorProficiencies |> Map.add Heavy Untrained }
                  |> pcEquipArmor armor

              isEqual actual expected

          testCase "can equip armor if strength and proficiency requirements met"
          <| fun () ->
              let armor =
                  { Id = "my-cool-armor"
                    Name = "Cool Armor"
                    Description = ""
                    Hardness = 0
                    MaterialHp = 0
                    BreakingThreshold = None
                    Category = Heavy
                    Level = 0
                    Rarity = Common
                    Traits = []
                    NormalBulk = Bulk 2
                    EquippedBulk = Bulk 2
                    AcBonus = 3
                    DexterityModCap = 2
                    StrengthRequirement = Some 14
                    CheckPenalty = None
                    SpeedPenalty = Some -5<ft>
                    Price = Some (gp 2) }

              let pc =
                  { newPc with
                      Strength = 16
                      ArmorProficiencies = newPc.ArmorProficiencies |> Map.add Heavy Trained }

              let expected : Result<PlayerCharacter, KarceroError> =
                  Ok { pc with Armor = Some armor }

              let actual = pc |> pcEquipArmor armor
              isEqual actual expected

          testCase "can be dealt strain damage"
          <| fun () ->
              let character =
                  { newPc with
                      Health =
                          { TemporaryHealth = 0
                            WoundThreshold = 10
                            WoundAmount = 0
                            StrainThreshold = 10
                            StrainAmount = 0 } }

              let damage =
                  [ { DamageType = PhysicalDamage Slashing
                      Amount = 10 } ]

              let expected =
                  { TotalDamage = 10
                    DealtDamage = damage
                    NewHealth =
                      { TemporaryHealth = 0
                        WoundThreshold = 10
                        WoundAmount = 0
                        StrainThreshold = 10
                        StrainAmount = 10 } }

              let actual = character |> pcCalculateDamageDealt damage

              isEqual actual expected

          testCase "can be dealt strain and wound damage"
          <| fun () ->
              let character =
                  { newPc with
                      Health =
                          { TemporaryHealth = 0
                            WoundThreshold = 10
                            WoundAmount = 0
                            StrainThreshold = 10
                            StrainAmount = 7 } }

              let damage =
                  [ { DamageType = PhysicalDamage Slashing
                      Amount = 10 } ]

              let expected =
                  { TotalDamage = 10
                    DealtDamage = damage
                    NewHealth =
                      { TemporaryHealth = 0
                        WoundThreshold = 10
                        WoundAmount = 7
                        StrainThreshold = 10
                        StrainAmount = 10 } }

              let actual = character |> pcCalculateDamageDealt damage

              isEqual actual expected

          testCase "can be dealt temp health, strain, and wound damage"
          <| fun () ->
              let character =
                  { newPc with
                      Health =
                          { TemporaryHealth = 2
                            WoundThreshold = 10
                            WoundAmount = 0
                            StrainThreshold = 10
                            StrainAmount = 7 } }

              let damage =
                  [ { DamageType = PhysicalDamage Slashing
                      Amount = 10 } ]

              let expected =
                  { TotalDamage = 10
                    DealtDamage = damage
                    NewHealth =
                      { TemporaryHealth = 0
                        WoundThreshold = 10
                        WoundAmount = 5
                        StrainThreshold = 10
                        StrainAmount = 10 } }

              let actual = character |> pcCalculateDamageDealt damage

              isEqual actual expected

          testCase "can be dealt more than max wound threshold"
          <| fun () ->
              let character =
                  { newPc with
                      Health =
                          { TemporaryHealth = 0
                            WoundThreshold = 10
                            WoundAmount = 10
                            StrainThreshold = 10
                            StrainAmount = 10 } }

              let damage =
                  [ { DamageType = PhysicalDamage Slashing
                      Amount = 10 } ]

              let expected =
                  { TotalDamage = 10
                    DealtDamage = damage
                    NewHealth =
                      { TemporaryHealth = 0
                        WoundThreshold = 10
                        WoundAmount = 20
                        StrainThreshold = 10
                        StrainAmount = 10 } }

              let actual = character |> pcCalculateDamageDealt damage

              isEqual actual expected

          testCase "cannot be dealt less than 1 damage"
          <| fun () ->
              let character =
                  { newPc with
                      Health =
                          { TemporaryHealth = 1
                            WoundThreshold = 10
                            WoundAmount = 0
                            StrainThreshold = 10
                            StrainAmount = 0 } }

              let damage =
                  [ { DamageType = PhysicalDamage Slashing
                      Amount = -1 } ]

              let expected =
                  { TotalDamage = 1
                    DealtDamage = damage
                    NewHealth =
                      { TemporaryHealth = 0
                        WoundThreshold = 10
                        WoundAmount = 0
                        StrainThreshold = 10
                        StrainAmount = 0 } }

              let actual = character |> pcCalculateDamageDealt damage

              isEqual actual expected ]
