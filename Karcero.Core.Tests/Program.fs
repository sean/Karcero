module Karcero.Core.Tests.Program

open Expecto

[<EntryPoint>]
let main argv = runTestsInAssembly defaultConfig argv
