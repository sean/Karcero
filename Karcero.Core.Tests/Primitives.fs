module Karcero.Core.Tests.Primitives

open Expecto
open Karcero.Core

[<Tests>]
let PrimitivesTests =
    testList
        "Primitives Tests"
        [ testCase "can determine casual dc modifier"
          <| fun () -> isEqual (dcModifier Casual) -1

          testCase "can determine tactical dc modifier"
          <| fun () -> isEqual (dcModifier Tactical) 0

          testCase "can determine punishing dc modifier"
          <| fun () -> isEqual (dcModifier Punishing) 1

          testCase "can determine mythical dc modifier"
          <| fun () -> isEqual (dcModifier Mythical) 2

          testCase "can determine miserable dc modifier"
          <| fun () -> isEqual (dcModifier Miserable) 3

          testCase "can determine ability mod from ability score 8"
          <| fun () -> isEqual (abilityModFromInt 8) -1

          testCase "can determine ability mod from ability score 9"
          <| fun () -> isEqual (abilityModFromInt 9) -1

          testCase "can determine ability mod from ability score 10"
          <| fun () -> isEqual (abilityModFromInt 10) 0

          testCase "can determine ability mod from ability score 11"
          <| fun () -> isEqual (abilityModFromInt 11) 0

          testCase "can determine ability mod from ability score 12"
          <| fun () -> isEqual (abilityModFromInt 12) 1

          testCase "can determine ability mod from ability score 14"
          <| fun () -> isEqual (abilityModFromInt 14) 2

          testCase "can determine ability mod from ability score 15"
          <| fun () -> isEqual (abilityModFromInt 15) 2

          testCase "can determine ability mod from ability score 18"
          <| fun () -> isEqual (abilityModFromInt 18) 4

          testCase "can determine ability mod from ability score 20"
          <| fun () -> isEqual (abilityModFromInt 20) 5

          testCase "can determine escalation value from escalation stage start"
          <| fun () -> isEqual (escalationValueFromStage EscalationStage.Start) 0

          testCase "can determine escalation value from escalation stage dangerous"
          <| fun () -> isEqual (escalationValueFromStage EscalationStage.Dangerous) 1

          testCase "can determine escalation value from escalation stage deadly"
          <| fun () -> isEqual (escalationValueFromStage EscalationStage.Deadly) 2

          testCase "can determine escalation value from escalation stage chaos"
          <| fun () -> isEqual (escalationValueFromStage EscalationStage.Chaos) 3

          testCase "default rounding should round down"
          <| fun () -> isEqual (round 1.999) 1

          testCase "can determine untrained proficiency bonus"
          <| fun () -> isEqual (proficiencyBonus Untrained) 0

          testCase "can determine trained proficiency bonus"
          <| fun () -> isEqual (proficiencyBonus Trained) 2

          testCase "can determine expert proficiency bonus"
          <| fun () -> isEqual (proficiencyBonus Expert) 4

          testCase "can determine master proficiency bonus"
          <| fun () -> isEqual (proficiencyBonus Master) 6

          testCase "can determine legendary proficiency bonus"
          <| fun () -> isEqual (proficiencyBonus Legendary) 8

          testCase "can determine when light is greater than bulk"
          <| fun () -> isGreaterThan (Light 11) (Bulk 1)

          testCase "can determine when light is less than bulk"
          <| fun () -> isLessThan (Light 9) (Bulk 1)

          testCase "can determine when light is isEqual to bulk"
          <| fun () -> isEqual (Light 10) (Bulk 1)

          testCase "can add light to bulk" <| fun () -> isEqual ((Light 1) + (Bulk 1)) 11

          testCase "can convert int to bulk amount"
          <| fun () ->
              isEqual (intToBulkAmount -20) (SimpleBulk Negligible)
              isEqual (intToBulkAmount 0) (SimpleBulk Negligible)
              isEqual (intToBulkAmount 1) (SimpleBulk (Light 1))
              isEqual (intToBulkAmount 10) (SimpleBulk (Bulk 1))
              isEqual (intToBulkAmount 32) (ComplexBulk { Bulk = 3 ; Light = 2 })

          testCase "can convert number of coins to bulk amount"
          <| fun () ->
              isEqual (coinsToBulkValue 0) Negligible
              isEqual (coinsToBulkValue 99) Negligible
              isEqual (coinsToBulkValue 100) Negligible
              isEqual (coinsToBulkValue 199) Negligible
              isEqual (coinsToBulkValue 200) Negligible
              isEqual (coinsToBulkValue 999) Negligible
              isEqual (coinsToBulkValue 1000) (Bulk 1)
              isEqual (coinsToBulkValue 1001) (Bulk 1)
              isEqual (coinsToBulkValue 1100) (Bulk 1)
              isEqual (coinsToBulkValue 2300) (Bulk 2)
              isEqual (coinsToBulkValue 9999) (Bulk 9)
              isEqual (coinsToBulkValue 10000) (Bulk 10)
              isEqual (coinsToBulkValue 10001) (Bulk 10)

          testCase "doesn't change dc without an adjustment"
          <| fun () -> isEqual (dc NoAdjustment 15) 15

          testCase "changes a dc with an incredibly easy adjustment"
          <| fun () -> isEqual (dc IncrediblyEasy 15) 5

          testCase "changes a dc with a very easy adjustment"
          <| fun () -> isEqual (dc VeryEasy 15) 10

          testCase "changes a dc with an easy adjustment"
          <| fun () -> isEqual (dc Easy 15) 13

          testCase "changes a dc with a hard adjustment"
          <| fun () -> isEqual (dc Hard 15) 17

          testCase "changes a dc with a very hard adjustment"
          <| fun () -> isEqual (dc VeryHard 15) 20

          testCase "changes a dc with an incredibly hard adjustment"
          <| fun () -> isEqual (dc IncrediblyHard 15) 25 ]
