module Karcero.Core.Tests.Items

open Expecto
open Karcero.Core
open Karcero.Core.Items

[<Tests>]
let ItemsTests =
    testList
        "Items tests"
        [ testCase "can't add an item to a physical storage inventory if it exceeds bulk value"
          <| fun () ->
              let storageItem =
                  { Id = "backpack"
                    Name = "Backpack"
                    Description = ""
                    Level = 0
                    Rarity = Common
                    Traits = []
                    NormalBulk = BulkValue.Light 1
                    EquippedBulk = BulkValue.Negligible
                    ReduceBulk = Some (Bulk 0)
                    MaxBulkCapacity = Some (Bulk 4)
                    Price = Some (sp 1) }

              let storedItem : GearItem =
                  { Id = "big-item"
                    Name = "Big Item"
                    Description = ""
                    Level = 0
                    Rarity = Common
                    Traits = []
                    NormalBulk = Bulk 3
                    EquippedBulk = Bulk 3
                    Price = None }

              let inventory =
                  { StorageItem = storageItem
                    Items = [ { Item = storedItem ; Amount = 1 } ] }

              let itemToAdd : GearItem =
                  { Id = "item-to-add"
                    Name = "Item to add"
                    Description = ""
                    Level = 0
                    Rarity = Common
                    Traits = []
                    NormalBulk = Bulk 2
                    EquippedBulk = Bulk 2
                    Price = None }

              let expected = Error (KarceroError.InventoryError ExceedsBulk)

              let actual = addToInventory itemToAdd inventory

              isEqual actual expected

          testCase "can add an item to a physical storage inventory if it does not exceeds bulk value"
          <| fun () ->
              let storageItem =
                  { Id = "backpack"
                    Name = "Backpack"
                    Description = ""
                    Level = 0
                    Rarity = Common
                    Traits = []
                    NormalBulk = BulkValue.Light 1
                    EquippedBulk = BulkValue.Negligible
                    ReduceBulk = Some (Bulk 0)
                    MaxBulkCapacity = Some (Bulk 4)
                    Price = Some (sp 1) }

              let storedItem : GearItem =
                  { Id = "big-item"
                    Name = "Big Item"
                    Description = ""
                    Level = 0
                    Rarity = Common
                    Traits = []
                    NormalBulk = Bulk 2
                    EquippedBulk = Bulk 2
                    Price = None }

              let inventory =
                  { StorageItem = storageItem
                    Items = [ { Item = storedItem ; Amount = 1 } ] }

              let itemToAdd : IItem =
                  { Id = "item-to-add"
                    Name = "Item to add"
                    Description = ""
                    Level = 0
                    Rarity = Common
                    Traits = []
                    NormalBulk = Bulk 2
                    EquippedBulk = Bulk 2
                    Price = None }

              let expected =
                  Ok { inventory with Items = { Item = itemToAdd ; Amount = 1 } :: inventory.Items }

              let actual = inventory |> addToInventory itemToAdd

              isEqual actual expected

          testCase
              "can add an item to an inventory if it does not exceed bulk and stack below max stack size already exists"
          <| fun () ->
              let storageItem =
                  { Id = "backpack"
                    Name = "Backpack"
                    Description = ""
                    Level = 0
                    Rarity = Common
                    Traits = []
                    NormalBulk = BulkValue.Light 1
                    EquippedBulk = BulkValue.Negligible
                    ReduceBulk = Some (Bulk 0)
                    MaxBulkCapacity = Some (Bulk 4)
                    Price = Some (sp 1) }

              let storedItem : GearItem =
                  { Id = "big-item"
                    Name = "Big Item"
                    Description = ""
                    Level = 0
                    Rarity = Common
                    Traits = []
                    NormalBulk = Bulk 2
                    EquippedBulk = Bulk 2
                    Price = None }

              let itemToAdd : IItem =
                  { Id = "item-to-add"
                    Name = "Item to add"
                    Description = ""
                    Level = 0
                    Rarity = Common
                    Traits = []
                    NormalBulk = Bulk 1
                    EquippedBulk = Bulk 1
                    Price = None }

              let inventory =
                  { StorageItem = storageItem
                    Items = [ { Item = storedItem ; Amount = 1 } ; { Item = itemToAdd ; Amount = 1 } ] }

              let expected =
                  Ok
                      { inventory with
                          Items = [ { Item = storedItem ; Amount = 1 } ; { Item = itemToAdd ; Amount = 2 } ] }

              let actual = inventory |> addToInventory itemToAdd

              isEqual actual expected ]
