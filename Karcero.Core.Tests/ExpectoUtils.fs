[<AutoOpen>]
module Karcero.Core.Tests.ExpectoUtils

open Expecto

let isEqual a b =
    Expect.equal a b $"{a} does not equal {b}"

let isTrue a = Expect.isTrue a $"{a} is not true"

let isGreaterThan a b =
    Expect.isGreaterThan a b $"{a} is not greater than {b}"

let isLessThan a b =
    Expect.isLessThan a b $"{a} is not less than {b}"
