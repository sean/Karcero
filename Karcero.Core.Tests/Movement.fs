module Karcero.Core.Tests.Movement

open Expecto
open Karcero.Core
open Movement

[<Tests>]
let MovementTests =
    testList
        "Movement Tests"
        [ testCase "can create an empty movement." <| fun () -> isEqual start Start

          testCase "can create a straight normal movement."
          <| fun () ->
              let actual = start |> add Straight Normal
              isEqual actual (Movement (Straight, Normal, Start))

          testCase "can create a straight difficult movement."
          <| fun () ->
              let actual = start |> add Straight Difficult
              isEqual actual (Movement (Straight, Difficult, Start))

          testCase "can create a straight greater difficult movement."
          <| fun () ->
              let actual = start |> add Straight GreaterDifficult

              isEqual actual (Movement (Straight, GreaterDifficult, Start))

          testCase "can create a diagonal normal movement."
          <| fun () ->
              let actual = start |> add Diagonal Normal
              isEqual actual (Movement (Diagonal, Normal, Start))

          testCase "can create a diagonal difficult movement."
          <| fun () ->
              let actual = start |> add Diagonal Difficult
              isEqual actual (Movement (Diagonal, Difficult, Start))

          testCase "can create a diagonal greater difficult movement."
          <| fun () ->
              let actual = start |> add Diagonal GreaterDifficult

              isEqual actual (Movement (Diagonal, GreaterDifficult, Start))

          testCase "can perform empty movement."
          <| fun () ->
              let actual = start |> calculateMovement 0<ft> 0

              isEqual actual 0<ft>

          testCase "can perform straight movement."
          <| fun () ->
              let actual =
                  start |> add Straight Normal |> add Straight Normal |> calculateMovement 0<ft> 0

              isEqual actual 10<ft>

          testCase "can perform diagonal movement."
          <| fun () ->
              let actual =
                  start |> add Diagonal Normal |> add Diagonal Normal |> calculateMovement 0<ft> 0

              isEqual actual 15<ft>

          testCase "can perform straight difficult terrain movement"
          <| fun () ->
              let actual = start |> add Straight Difficult |> calculateMovement 0<ft> 0

              isEqual actual 10<ft>

          testCase "can perform diagonal difficult terrain movement"
          <| fun () ->
              let actual =
                  start
                  |> add Diagonal Difficult
                  |> add Diagonal Difficult
                  |> calculateMovement 0<ft> 0

              isEqual actual 25<ft>

          testCase "can perform straight greater difficult terrain movement"
          <| fun () ->
              let actual =
                  start
                  |> add Straight GreaterDifficult
                  |> add Straight GreaterDifficult
                  |> calculateMovement 0<ft> 0

              isEqual actual 30<ft>

          testCase "can perform diagonal greater difficult terrain movement"
          <| fun () ->
              let actual =
                  start
                  |> add Diagonal GreaterDifficult
                  |> add Diagonal GreaterDifficult
                  |> calculateMovement 0<ft> 0

              isEqual actual 35<ft>

          testCase "can perform complex movement scenario 1a"
          <| fun () ->
              let actual =
                  start
                  |> add Straight Normal
                  |> add Diagonal Normal
                  |> add Diagonal Normal
                  |> add Diagonal Difficult
                  |> add Straight Difficult
                  |> calculateMovement 0<ft> 0

              isEqual actual 40<ft>

          testCase "can perform complex movement scenario 1b"
          <| fun () ->
              let actual =
                  start
                  |> add Straight Normal
                  |> add Diagonal Normal
                  |> add Diagonal Normal
                  |> add Diagonal Difficult
                  |> add Diagonal Difficult
                  |> calculateMovement 0<ft> 0

              isEqual actual 45<ft> ]
