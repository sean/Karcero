// Gonna be honest, I don't even know if this game will have a party.
namespace Karcero.Core

type PartyCharacter = PartyCharacter of PlayerCharacter
type MainCharacter = MainCharacter of PartyCharacter
type Slot1Character = Slot1Character of PartyCharacter
type Slot2Character = Slot2Character of PartyCharacter
type Slot3Character = Slot3Character of PartyCharacter

type Party =
    { MainCharacter : MainCharacter
      PartySlot1 : Slot1Character option
      PartySlot2 : Slot2Character option
      PartySlot3 : Slot3Character option
      ActiveCharacter : PartyCharacter }

module PartySystem =
    let private switchActiveCharacter newActiveCharacter party =
        { party with ActiveCharacter = newActiveCharacter }

    let private playerToPartyCharacter pc = PartyCharacter pc
    let private toMain character = MainCharacter character

    let private toSlot1 = playerToPartyCharacter >> Slot1Character
    let private toSlot2 = playerToPartyCharacter >> Slot2Character
    let private toSlot3 = playerToPartyCharacter >> Slot3Character

    let addCharacterToParty (c : PlayerCharacter) p =
        match p.PartySlot1, p.PartySlot2, p.PartySlot3 with
        // Make sure character somehow isn't already in the party
        | Some (Slot1Character (PartyCharacter x)), _, _ when c = x -> Error (PartyError AlreadyInParty)
        | _, Some (Slot2Character (PartyCharacter x)), _ when c = x -> Error (PartyError AlreadyInParty)
        | _, _, Some (Slot3Character (PartyCharacter x)) when c = x -> Error (PartyError AlreadyInParty)
        // Add character to first open slot
        | None, _, _ -> Ok { p with PartySlot1 = Some (c |> toSlot1) }
        | _, None, _ -> Ok { p with PartySlot2 = Some (c |> toSlot2) }
        | _, _, None -> Ok { p with PartySlot3 = Some (c |> toSlot3) }
        // If no slots are available, we can't add them
        | Some _, Some _, Some _ -> Error (PartyError NoSlotsRemaining)

    let removeSlot1FromParty party = { party with PartySlot1 = None }
    let removeSlot2FromParty party = { party with PartySlot2 = None }
    let removeSlot3FromParty party = { party with PartySlot3 = None }

    let getPartyCharacter pc =
        match pc with
        | PartyCharacter character -> character

    let getMainCharacter mc =
        match mc with
        | MainCharacter (PartyCharacter character) -> character

    let getSlot1Character slotCharacter =
        match slotCharacter with
        | Slot1Character (PartyCharacter character) -> character

    let getSlot2Character slotCharacter =
        match slotCharacter with
        | Slot2Character (PartyCharacter character) -> character

    let getSlot3Character slotCharacter =
        match slotCharacter with
        | Slot3Character (PartyCharacter character) -> character

    let makeMainCharacterActive mc party =
        let (MainCharacter pc) = mc
        switchActiveCharacter pc party

    let makeSlot1CharacterActive mc party =
        let (Slot1Character pc) = mc
        switchActiveCharacter pc party

    let makeSlot2CharacterActive mc party =
        let (Slot2Character pc) = mc
        switchActiveCharacter pc party

    let makeSlot3CharacterActive mc party =
        let (Slot3Character pc) = mc
        switchActiveCharacter pc party

    let createParty mainCharacter =
        let partyMc = playerToPartyCharacter mainCharacter

        { MainCharacter = toMain partyMc
          PartySlot1 = None
          PartySlot2 = None
          PartySlot3 = None
          ActiveCharacter = partyMc }
