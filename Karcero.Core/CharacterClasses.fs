namespace Karcero.Core

type AttackSkill =
    | WeaponSkill of WeaponSkills
    | OtherAttackSkill of name : string

type DefensiveSkill =
    | ArmorSkill of ArmorSkills
    | OtherDefensiveSkill of name : string

type TrainedSkills =
    { Skills : Set<Skill>
      Additional : int }

type ICharacterClass =
    inherit IBaseUnleveledData
    inherit IRareItem
    inherit IProvidesTraits

type CharacterClass =
    { Id : string
      Name : string
      Description : string
      Rarity : Rarity
      InitialPerception : Proficiency
      Traits : Trait list
      ProvidesTraits : Trait list
      SavingThrows : Map<SavingThrows, Proficiency>
      Attacks : Map<AttackSkill, Proficiency>
      Defenses : Map<DefensiveSkill, Proficiency>
      ClassDc : Proficiency
      TrainedSkills : TrainedSkills
      AncestryFeatLevels : LevelSet
      ClassFeatLevels : LevelSet
      GeneralFeatLevels : LevelSet
      SkillFeatLevels : LevelSet
      SkillIncreaseLevels : LevelSet }
    interface ICharacterClass with
        member this.Id = this.Id
        member this.Name = this.Name
        member this.Traits = this.Traits
        member this.Rarity = this.Rarity

        member this.ProvidesTraits = this.ProvidesTraits

module CharacterClasses =
    let newCharacterClass () =
        { Id = $"CharacterClass.{System.Guid.NewGuid ()}"
          Name = "New class"
          Description = ""
          Rarity = Common
          InitialPerception = Untrained
          Traits = []
          ProvidesTraits = []
          SavingThrows = Map.empty
          Attacks = Map.empty
          Defenses = Map.empty
          ClassDc = Trained
          TrainedSkills = { Skills = Set.empty ; Additional = 0 }
          AncestryFeatLevels = Set.empty
          ClassFeatLevels = Set.empty
          GeneralFeatLevels = Set.empty
          SkillFeatLevels = Set.empty
          SkillIncreaseLevels = Set.empty }
