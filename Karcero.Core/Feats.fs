namespace Karcero.Core

type FeatCategory =
    | General
    | Skill
    | Class
    | Ancestry
    | Archetype
    | DeityBoon
    | DeityCurse
    | ClassFeature
    | AncestryFeature

type Feat =
    { Id : string
      Name : string
      Traits : Trait list
      Rarity : Rarity
      Category : FeatCategory
      Level : int
      ActionCost : ActionCost option }
    interface IBaseLeveledData with
        member this.Id = this.Id
        member this.Name = this.Name
        member this.Level = this.Level
        member this.Traits = this.Traits

    interface IRareItem with
        member this.Rarity = this.Rarity

type GrantingFeat =
    { Feat : Feat
      Grants : GrantingFeat list }
