namespace Karcero.Core

type Die =
    | D4
    | D6
    | D8
    | D10
    | D12
    | D20
    member this.NumFaces () =
        match this with
        | D4 -> 4
        | D6 -> 6
        | D8 -> 8
        | D10 -> 10
        | D12 -> 12
        | D20 -> 20

type DicePool =
    { NumD4 : int
      NumD6 : int
      NumD8 : int
      NumD10 : int
      NumD12 : int
      NumD20 : int }

type DieResult = { Die : Die ; Result : int }

type RollResult = DieResult list

module Dice =
    let roll (die : Die) =
        System.Random().Next (1, die.NumFaces ())
