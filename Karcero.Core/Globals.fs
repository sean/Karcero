namespace Karcero.Core

type DifficultySetting =
    | Casual
    | Tactical
    | Punishing
    | Mythical
    | Miserable

type Configuration = { Difficulty : DifficultySetting }

type ConfigurationState(cfg) =
    member val Difficulty : DifficultySetting = cfg.Difficulty with get, set
    public new(cfg) = ConfigurationState cfg

[<AutoOpen>]
module Globals =
    let defaultSystemConfiguration = { Difficulty = Tactical }

    let systemConfiguration = ConfigurationState defaultSystemConfiguration
