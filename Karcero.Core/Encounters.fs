namespace Karcero.Core

type InitiativeSkill =
    | Perception
    | Skill of Skill

type InitiativeCharacter =
    | NonPlayerCharacter of NonPlayerCharacter
    | PlayerCharacter of PlayerCharacter

type Initiative =
    { Character : InitiativeCharacter
      Result : int
      SkillUsed : InitiativeSkill }

type Encounter =
    { CurrentRound : int
      CurrentInitiative : Initiative
      InitiativeOrder : Initiative list }

module Encounters =
    open Actors

    let characterRollInitiative skill bonuses penalties character =
        { Character = character
          SkillUsed = skill
          Result =
            match character, skill with
            | PlayerCharacter pc, Perception -> (pcRollPerception bonuses penalties pc).TotalResult
            | PlayerCharacter pc, Skill skill -> (pcRollSkillCheck skill bonuses penalties pc).TotalResult
            | NonPlayerCharacter npc, Perception -> npcRollPerception bonuses penalties npc
            | NonPlayerCharacter npc, Skill skill -> npcRollSkill skill bonuses penalties npc }

    let rollWithSkill skill = characterRollInitiative (Skill skill)

    let rollPerceptionInitiative = characterRollInitiative Perception

    let rollDeceptionInitiative = rollWithSkill (CharismaSkill Deception)

    let rollDiplomacyInitiative = rollWithSkill (CharismaSkill Diplomacy)
