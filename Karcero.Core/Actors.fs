namespace Karcero.Core

type NpcHealth =
    { TemporaryHealth : int
      WoundAmount : int
      WoundThreshold : int }

type PcHealth =
    { TemporaryHealth : int
      WoundThreshold : int
      WoundAmount : int
      StrainThreshold : int
      StrainAmount : int }

type DieAt =
    | Dying of int
    | Instant

type IKillable<'a> =
    abstract member DieAt : DieAt
    abstract member Health : 'a

/// Adjustments are a net change,
/// not the final number.
type NpcAdjustments =
    { AdjLevel : int
      AdjArmorClass : int
      AdjAttackModifier : int
      AdjSavingThrows : int
      AdjDCs : int
      AdjPerception : int
      AdjSkills : int
      AdjWoundThreshold : int }

type BonusTotals =
    { Circumstance : int
      Innate : int
      Proficiency : int
      Status : int
      Item : int
      Bonuses : Bonus list }

type PenaltyTotals =
    { Circumstance : int
      Untyped : int list
      Status : int
      Item : int
      Penalties : Penalty list }

type IActor<'a> =
    inherit IBaseLeveledData
    inherit IKillable<'a>

type ICharacter<'a> =
    inherit IActor<'a>
    abstract member Size : CharacterSize
    abstract member Languages : string list
    abstract member Movement : Map<MovementType, int<ft>>
    abstract member PreciseSenses : Set<Sense>
    /// Imprecise senses.
    abstract member ImpSenses : Set<Sense>
    abstract member VagueSense : Set<Sense>
    abstract member Conditions : Set<Condition>

type NonPlayerCharacter =
    { Id : string
      Name : string
      Level : int
      Rarity : Rarity
      Health : NpcHealth
      DieAt : DieAt
      Traits : Trait list
      ArmorClass : int
      Size : CharacterSize
      Languages : string list
      // Will saving throw modifier.
      WillSaveMod : int
      // Reflex saving throw modifier.
      RefSaveMod : int
      // Fortitude saving throw modifier.
      FortSaveMod : int
      Movement : Map<MovementType, int<ft>>
      PreciseSenses : Set<Sense>
      // Imprecise Senses
      ImpSenses : Set<Sense>
      VagueSense : Set<Sense>
      Conditions : Set<Condition>
      InventoryItems : IItem list
      PerceptionModifier : int
      StrengthModifier : int
      DexterityModifier : int
      ConstitutionModifier : int
      IntelligenceModifier : int
      WisdomModifier : int
      CharismaModifier : int
      StrengthSkillModifiers : Map<StrengthSkill, int>
      DexteritySkillModifiers : Map<DexteritySkill, int>
      ConstitutionSkillModifiers : Map<ConstitutionSkill, int>
      IntelligenceSkillModifiers : Map<IntelligenceSkill, int>
      WisdomSkillModifiers : Map<WisdomSkill, int>
      CharismaSkillModifiers : Map<CharismaSkill, int>
      Adjustments : NpcAdjustments }

    interface ICharacter<NpcHealth> with
        member this.Id = this.Id
        member this.Name = this.Name
        member this.Level = this.Level
        member this.Traits = this.Traits
        member this.Size = this.Size
        member this.DieAt = this.DieAt
        member this.Health = this.Health
        member this.Languages = this.Languages
        member this.Movement = this.Movement
        member this.Conditions = this.Conditions
        member this.PreciseSenses = this.PreciseSenses
        member this.ImpSenses = this.ImpSenses
        member this.VagueSense = this.VagueSense

    interface IRareItem with
        member this.Rarity = this.Rarity

type Momentum = { PoolSize : int ; Used : int }

type PlayerCharacter =
    { Id : string
      Name : string
      Level : int
      Traits : Trait list
      Size : CharacterSize
      Languages : string list
      Feats : Feat list
      Class : CharacterClass
      // Will saving throw proficiency.
      WillSaveProf : Proficiency
      // Reflex saving throw proficiency.
      RefSaveProf : Proficiency
      // Fortitude saving throw proficiency.
      FortSaveProf : Proficiency
      Movement : Map<MovementType, int<ft>>
      PreciseSenses : Set<Sense>
      // Imprecise senses.
      ImpSenses : Set<Sense>
      VagueSense : Set<Sense>
      KeyAbility : Ability
      Conditions : Set<Condition>
      DieAt : DieAt
      Health : PcHealth
      Momentum : Momentum
      // TODO: Create ancestries. Maybe?
      Ancestry : string
      // TODO: Create backgrounds.
      Background : string
      ExperiencePoints : int
      WornItems : WornItem list
      Armor : ArmorItem option
      Resolve : int
      PerceptionProficiency : Proficiency
      Strength : int
      Dexterity : int
      Constitution : int
      Intelligence : int
      Wisdom : int
      Charisma : int
      ArmorProficiencies : Map<ArmorCategory, Proficiency>
      // Strength skill proficiencies.
      StrSkillProficiencies : Map<StrengthSkill, Proficiency>
      // Dexterity skill proficiencies.
      DexSkillProficiencies : Map<DexteritySkill, Proficiency>
      // Constitution skill proficiencies.
      ConSkillProficiencies : Map<ConstitutionSkill, Proficiency>
      // Intelligence skill proficiencies.
      IntSkillProficiencies : Map<IntelligenceSkill, Proficiency>
      // Wisdom skill proficiencies.
      WisSkillProficiencies : Map<WisdomSkill, Proficiency>
      // Charisma skill proficiencies.
      ChaSkillProficiencies : Map<CharismaSkill, Proficiency> }

    interface ICharacter<PcHealth> with
        member this.Id = this.Id
        member this.Name = this.Name
        member this.Level = this.Level
        member this.Traits = this.Traits
        member this.Size = this.Size
        member this.DieAt = this.DieAt
        member this.Health = this.Health
        member this.Languages = this.Languages
        member this.Conditions = this.Conditions
        member this.Movement = this.Movement
        member this.PreciseSenses = this.PreciseSenses
        member this.ImpSenses = this.ImpSenses
        member this.VagueSense = this.VagueSense

type PlayerCheckResult =
    { DieResult : int
      TotalResult : int
      Ability : Ability
      AbilityScore : int
      AbilityMod : int
      FinalBonus : int
      FinalPenalty : int
      BonusTotals : BonusTotals
      PenaltyTotals : PenaltyTotals }

type MultipleAttackPenaltyCount =
    | First
    | Second
    | ThirdOrSubsequent

type CharacterDamageDealt<'a> =
    { DealtDamage : DealtDamage
      TotalDamage : int
      NewHealth : 'a }

type PcDamageDealt = CharacterDamageDealt<PcHealth>
type NpcDamageDealt = CharacterDamageDealt<NpcHealth>

module Actors =
    open CharacterClasses
    open Dice
    open Movement

    let private bonusesToTotals bonuses =
        bonuses
        |> List.fold
            (fun state bonus ->
                match bonus with
                | InnateBonus n -> { state with Innate = max n state.Innate }
                | CircumstanceBonus n -> { state with Circumstance = max n state.Circumstance }
                | ProficiencyBonus n -> { state with Proficiency = max n state.Proficiency }
                | StatusBonus n -> { state with Status = max n state.Status }
                | ItemBonus n -> { state with Item = max n state.Item })
            { Circumstance = 0
              Proficiency = 0
              Innate = 0
              Status = 0
              Item = 0
              Bonuses = bonuses }

    let private totalBonus bonuses =
        let totals = bonusesToTotals bonuses

        totals.Circumstance + totals.Item + totals.Proficiency + totals.Status, totals

    let private penaltiesToTotals penalites =
        penalites
        |> List.fold
            (fun state bonus ->
                match bonus with
                | UntypedPenalty n -> { state with Untyped = n :: state.Untyped }
                | CircumstancePenalty n -> { state with Circumstance = min n state.Circumstance }
                | StatusPenalty n -> { state with Status = min n state.Status }
                | ItemPenalty n -> { state with Item = min n state.Item })
            { Circumstance = 0
              Untyped = []
              Status = 0
              Item = 0
              Penalties = penalites }

    let private totalPenalty penalties =
        let totals = penaltiesToTotals penalties

        (totals.Circumstance + totals.Item + totals.Status + (List.sum totals.Untyped), totals)

    let characterReach (character : ICharacter<_>) = sizeToReach character.Size

    let characterSpace (character : ICharacter<_>) = sizeToSpace character.Size

    let actionCostForMovement soFarMoved diagonals movement movementType (character : ICharacter<_>) =
        let movement = calculateMovement soFarMoved diagonals movement

        character.Movement
        |> Map.tryFind movementType
        |> Option.map (fun speed -> movementToActions speed movement)
        |> Option.defaultValue 0<actions>

    let adjustNpc npc adjustments = { npc with Adjustments = adjustments }

    let npcRollCheckWithInfo bonuses penalties flatBonus =
        let penaltyTotal, penaltyMeta = totalPenalty penalties

        let bonusTotal, bonusMeta = totalBonus bonuses

        (roll D20 + flatBonus + bonusTotal + penaltyTotal, bonusMeta, penaltyMeta)

    let npcRollCheck bonuses penalties flatBonus =
        let result, _, _ = npcRollCheckWithInfo bonuses penalties flatBonus

        result

    let private npcSkillToModifier npc skill =
        let bonus =
            match skill with
            | StrengthSkill skill -> Map.tryFind skill npc.StrengthSkillModifiers
            | DexteritySkill skill -> Map.tryFind skill npc.DexteritySkillModifiers
            | ConstitutionSkill skill -> Map.tryFind skill npc.ConstitutionSkillModifiers
            | IntelligenceSkill skill -> Map.tryFind skill npc.IntelligenceSkillModifiers
            | WisdomSkill skill -> Map.tryFind skill npc.WisdomSkillModifiers
            | CharismaSkill skill -> Map.tryFind skill npc.CharismaSkillModifiers

        Option.defaultValue 0 bonus

    let npcRollSkill skill bonuses penalties npc =
        let bonus = npcSkillToModifier npc skill

        npcRollCheck bonuses penalties bonus

    let npcRollPerception bonuses penalties npc =
        npcRollCheck bonuses penalties npc.PerceptionModifier

    let playerProficiencyBonus proficiency pc =
        match proficiency with
        | Untrained -> 0
        | p -> pc.Level + proficiencyBonus p

    let newPc =
        { Id = $"PlayerCharacter.{System.Guid.NewGuid ()}"
          Name = "New player"
          Level = 1
          Traits = []
          Size =
            { Category = CharacterSizeCategory.Medium
              Orientation = Tall }
          Languages = [ "common" ]
          Feats = []
          Class = newCharacterClass ()
          WillSaveProf = Untrained
          RefSaveProf = Untrained
          FortSaveProf = Untrained
          Movement = Map.empty |> Map.add Speed 25<ft>
          PreciseSenses = Set.empty
          ImpSenses = Set.empty
          VagueSense = Set.empty
          Conditions = Set.empty
          Momentum = { PoolSize = 2 ; Used = 0 }
          DieAt = Dying 4
          Health =
            { TemporaryHealth = 0
              WoundThreshold = 0
              WoundAmount = 0
              StrainThreshold = 0
              StrainAmount = 0 }
          Ancestry = ""
          Background = ""
          ExperiencePoints = 0
          WornItems = []
          Resolve = 0
          PerceptionProficiency = Master
          Strength = 10
          Dexterity = 10
          Constitution = 10
          Intelligence = 10
          Wisdom = 10
          Charisma = 10
          Armor = None
          ArmorProficiencies = Map.empty
          StrSkillProficiencies = Map.empty
          DexSkillProficiencies = Map.empty
          ConSkillProficiencies = Map.empty
          IntSkillProficiencies = Map.empty |> Map.add Arcane Legendary
          WisSkillProficiencies = Map.empty
          ChaSkillProficiencies = Map.empty
          KeyAbility = Strength }

    let calculatePcArmorClass pc =
        let baseAc = 10
        let playerArmor = pc.Armor

        let dexModCap =
            playerArmor
            |> Option.map (fun armor -> armor.DexterityModCap)
            |> Option.defaultValue 0

        let playerDexMod = pc.Dexterity |> abilityModFromInt

        let finalDexMod = System.Math.Clamp (playerDexMod, playerDexMod, dexModCap)

        let playerArmorProficiencyBonus =
            playerArmor
            |> Option.map (fun armor ->
                pc.ArmorProficiencies
                |> Map.tryFind armor.Category
                |> Option.defaultValue Untrained)
            |> Option.defaultValue Untrained
            |> (fun p -> playerProficiencyBonus p pc)

        let playerArmorAcBonus =
            playerArmor |> Option.map (fun a -> a.AcBonus) |> Option.defaultValue 0

        baseAc + finalDexMod + playerArmorProficiencyBonus + playerArmorAcBonus

    let pcRollCheck bonuses penalties abilityMod abilityScore ability =
        let bonusTotal, bonusMeta = totalBonus bonuses

        let penaltyTotal, penaltyMeta = totalPenalty penalties

        let rollResult = roll D20

        { DieResult = rollResult
          TotalResult = rollResult + abilityMod + bonusTotal + penaltyTotal
          FinalBonus = bonusTotal
          FinalPenalty = penaltyTotal
          BonusTotals = bonusMeta
          PenaltyTotals = penaltyMeta
          AbilityMod = abilityMod
          AbilityScore = abilityScore
          Ability = ability }

    let pcRollPerception bonuses penalties pc =
        let proficiencyBonus = playerProficiencyBonus pc.PerceptionProficiency pc

        let ability = pc.Wisdom
        let abilityMod = abilityModFromInt ability

        pcRollCheck (ProficiencyBonus proficiencyBonus :: bonuses) penalties abilityMod ability Wisdom

    let pcGetSkillBonus skill pc =
        let proficiency =
            match skill with
            | StrengthSkill skill -> Map.tryFind skill pc.StrSkillProficiencies
            | DexteritySkill skill -> Map.tryFind skill pc.DexSkillProficiencies
            | ConstitutionSkill skill -> Map.tryFind skill pc.ConSkillProficiencies
            | IntelligenceSkill skill -> Map.tryFind skill pc.IntSkillProficiencies
            | WisdomSkill skill -> Map.tryFind skill pc.WisSkillProficiencies
            | CharismaSkill skill -> Map.tryFind skill pc.ChaSkillProficiencies

        proficiency |> Option.defaultValue Untrained |> proficiencyBonus

    let pcGetAbilityScoreFromAbility pc ability =
        match ability with
        | Strength -> pc.Strength
        | Dexterity -> pc.Dexterity
        | Constitution -> pc.Constitution
        | Intelligence -> pc.Intelligence
        | Wisdom -> pc.Wisdom
        | Charisma -> pc.Charisma

    let pcRollSkillCheck skill bonuses penalties pc =
        let skillProficiency = pcGetSkillBonus skill pc

        let ability = skill |> skillToAbility

        let abilityScore = ability |> pcGetAbilityScoreFromAbility pc

        let abilityModifier = abilityScore |> abilityModFromInt

        let proficiencyBonus = pc.Level + skillProficiency

        pcRollCheck (ProficiencyBonus proficiencyBonus :: bonuses) penalties abilityModifier abilityScore ability

    let multipleAttackPenalty attack usingAgileWeapon =
        match attack, usingAgileWeapon with
        // All first attacks are not penalized
        | First, _ -> UntypedPenalty 0
        // Standard weapons
        | Second, false -> UntypedPenalty -5
        | ThirdOrSubsequent, false -> UntypedPenalty -10
        // Agile weapons
        | Second, true -> UntypedPenalty -4
        | ThirdOrSubsequent, true -> UntypedPenalty -8

    let pcEquipArmor (armor : ArmorItem) pc =
        let meetsStrRequirement =
            armor.StrengthRequirement
            |> Option.map (fun strRequirement -> pc.Strength >= strRequirement)
            |> Option.defaultValue true

        let meetsProficiencyRequirement =
            pc.ArmorProficiencies
            |> Map.tryFind armor.Category
            |> Option.map (fun prof -> prof >= Trained)
            |> Option.defaultValue false

        match meetsStrRequirement, meetsProficiencyRequirement with
        | true, true -> Ok { pc with Armor = Some armor }
        | false, _ -> Error (ArmorEquipError StrTooLow)
        | _, false -> Error (ArmorEquipError ProfTooLow)

    let private calculateTotalDealtDamage (damage : DealtDamage) =
        damage |> List.map (fun dmg -> dmg.Amount) |> List.sum

    let pcCalculateDamageDealt (damage : DealtDamage) pc =
        let damageTotal = calculateTotalDealtDamage damage

        let damageTotal = System.Math.Clamp (damageTotal, 1, max damageTotal 1)

        let newTempHp =
            System.Math.Clamp (pc.Health.TemporaryHealth - damageTotal, 0, pc.Health.TemporaryHealth)

        let tempHpRemoved = abs (newTempHp - pc.Health.TemporaryHealth)

        let damageTotalAfterTempHp = damageTotal - tempHpRemoved

        let newStrain =
            System.Math.Clamp (pc.Health.StrainAmount + damageTotalAfterTempHp, 0, pc.Health.StrainThreshold)

        let strainAdded = abs (newStrain - pc.Health.StrainAmount)

        let damageTotalAfterStrain = damageTotalAfterTempHp - strainAdded

        let newWounds = pc.Health.WoundAmount + damageTotalAfterStrain

        { TotalDamage = damageTotal
          DealtDamage = damage
          NewHealth =
            { pc.Health with
                TemporaryHealth = newTempHp
                StrainAmount = newStrain
                WoundAmount = newWounds } }
