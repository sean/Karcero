namespace Karcero.Core

type IBreakable =
    abstract member BreakingThreshold : int option
    abstract member MaterialHp : int
    abstract member Hardness : int

type IItem =
    inherit IBaseUnleveledData
    abstract member Description : string
    abstract member Price : Price option
    abstract member NormalBulk : BulkValue

type ReadableItem =
    { Id : string
      Name : string
      Description : string
      Traits : Trait list
      NormalBulk : BulkValue
      Price : Price option
      Contents : string }
    interface IItem with
        member this.Id = this.Id
        member this.Name = this.Name
        member this.Description = this.Description
        member this.Traits = this.Traits
        member this.NormalBulk = this.NormalBulk
        member this.Price = this.Price

type TreasureItem =
    { Id : string
      Name : string
      Description : string
      Traits : Trait list
      NormalBulk : BulkValue
      Price : Price option }
    interface IItem with
        member this.Id = this.Id
        member this.Name = this.Name
        member this.Description = this.Description
        member this.Traits = this.Traits
        member this.NormalBulk = this.NormalBulk
        member this.Price = this.Price

type IEquippableItem =
    inherit IItem
    inherit IBaseLeveledData
    inherit IRareItem
    abstract member EquippedBulk : BulkValue

type ItemWielding =
    | OneHand
    | OneOrTwoHand
    | TwoHand

type IHeldItem =
    inherit IEquippableItem
    abstract member HeldWith : ItemWielding

type HeldItem =
    { Id : string
      Name : string
      Description : string
      Level : int
      Rarity : Rarity
      Traits : Trait list
      NormalBulk : BulkValue
      EquippedBulk : BulkValue
      Price : Price option
      HeldWith : ItemWielding }
    interface IHeldItem with
        member this.Id = this.Id
        member this.Name = this.Name
        member this.Description = this.Description
        member this.Level = this.Level
        member this.Rarity = this.Rarity
        member this.Traits = this.Traits
        member this.NormalBulk = this.NormalBulk
        member this.EquippedBulk = this.EquippedBulk
        member this.Price = this.Price
        member this.HeldWith = this.HeldWith

type WeaponItemDamage = { Die : Die ; NumDice : int }

type WeaponGroup =
    | Axe
    | Bomb
    | Bow
    | Brawling
    | Club
    | Dart
    | Firearm
    | Flail
    | Hammer
    | Knife
    | Pick
    | Polearm
    | Shield
    | Sling
    | Spear
    | Sword
    | Other of name : string

type WeaponCategory =
    | Unarmed
    | Simple
    | Martial
    | Advanced

type WeaponItem =
    { Id : string
      Name : string
      Description : string
      Hardness : int
      BreakingThreshold : int option
      MaterialHp : int
      Level : int
      Rarity : Rarity
      Traits : Trait list
      NormalBulk : BulkValue
      EquippedBulk : BulkValue
      HeldWith : ItemWielding
      Damage : WeaponItemDamage
      Price : Price option }
    interface IHeldItem with
        member this.Id = this.Id
        member this.Name = this.Name
        member this.Description = this.Description
        member this.Level = this.Level
        member this.Rarity = this.Rarity
        member this.Traits = this.Traits
        member this.NormalBulk = this.NormalBulk
        member this.EquippedBulk = this.EquippedBulk
        member this.Price = this.Price
        member this.HeldWith = this.HeldWith

    interface IBreakable with
        member this.Hardness = this.Hardness
        member this.BreakingThreshold = this.BreakingThreshold
        member this.MaterialHp = this.MaterialHp

type ArmorCategory =
    | Unarmored
    | Light
    | Medium
    | Heavy

type WornSlot =
    | Gloves
    | Headware
    | Face
    | Shoes
    | Cloak
    | Belt
    | Anklets
    | Circlet
    | Worn

type GearItem =
    { Id : string
      Name : string
      Description : string
      Level : int
      Rarity : Rarity
      Traits : Trait list
      NormalBulk : BulkValue
      EquippedBulk : BulkValue
      Price : Price option }
    interface IEquippableItem with
        member this.Id = this.Id
        member this.Name = this.Name
        member this.Description = this.Description
        member this.Level = this.Level
        member this.Rarity = this.Rarity
        member this.Traits = this.Traits
        member this.NormalBulk = this.NormalBulk
        member this.EquippedBulk = this.EquippedBulk
        member this.Price = this.Price

type WornItem =
    { Id : string
      Name : string
      Description : string
      Level : int
      Rarity : Rarity
      Traits : Trait list
      NormalBulk : BulkValue
      EquippedBulk : BulkValue
      Price : Price option
      WornSlot : WornSlot }
    interface IEquippableItem with
        member this.Id = this.Id
        member this.Name = this.Name
        member this.Description = this.Description
        member this.Level = this.Level
        member this.Rarity = this.Rarity
        member this.Traits = this.Traits
        member this.NormalBulk = this.NormalBulk
        member this.EquippedBulk = this.EquippedBulk
        member this.Price = this.Price

type ArmorItem =
    { Id : string
      Name : string
      Description : string
      Hardness : int
      BreakingThreshold : int option
      MaterialHp : int
      Category : ArmorCategory
      Level : int
      Rarity : Rarity
      Traits : Trait list
      NormalBulk : BulkValue
      EquippedBulk : BulkValue
      AcBonus : int
      DexterityModCap : int
      StrengthRequirement : int option
      CheckPenalty : int option
      SpeedPenalty : int<ft> option
      Price : Price option }
    interface IEquippableItem with
        member this.Id = this.Id
        member this.Name = this.Name
        member this.Description = this.Description
        member this.Level = this.Level
        member this.Rarity = this.Rarity
        member this.Traits = this.Traits
        member this.NormalBulk = this.NormalBulk
        member this.EquippedBulk = this.EquippedBulk
        member this.Price = this.Price

    interface IBreakable with
        member this.Hardness = this.Hardness
        member this.BreakingThreshold = this.BreakingThreshold
        member this.MaterialHp = this.MaterialHp

type ShieldItem =
    { Id : string
      Name : string
      Description : string
      Hardness : int
      BreakingThreshold : int option
      MaterialHp : int
      Category : ArmorCategory
      Level : int
      Rarity : Rarity
      Traits : Trait list
      NormalBulk : BulkValue
      EquippedBulk : BulkValue
      AcBonus : int
      DexterityModCap : int
      StrengthRequirement : int option
      CheckPenalty : int option
      SpeedPenalty : int<ft> option
      Price : Price option
      ShieldWeapon : WeaponItem }
    interface IEquippableItem with
        member this.Id = this.Id
        member this.Name = this.Name
        member this.Description = this.Description
        member this.Level = this.Level
        member this.Rarity = this.Rarity
        member this.Traits = this.Traits
        member this.NormalBulk = this.NormalBulk
        member this.EquippedBulk = this.EquippedBulk
        member this.Price = this.Price

    interface IBreakable with
        member this.Hardness = this.Hardness
        member this.BreakingThreshold = this.BreakingThreshold
        member this.MaterialHp = this.MaterialHp

type PhysicalStorageItem =
    { Id : string
      Name : string
      Description : string
      Level : int
      Rarity : Rarity
      Traits : Trait list
      NormalBulk : BulkValue
      EquippedBulk : BulkValue
      ReduceBulk : BulkValue option
      MaxBulkCapacity : BulkValue option
      Price : Price option }
    interface IEquippableItem with
        member this.Id = this.Id
        member this.Name = this.Name
        member this.Description = this.Description
        member this.Level = this.Level
        member this.Rarity = this.Rarity
        member this.Traits = this.Traits
        member this.NormalBulk = this.NormalBulk
        member this.EquippedBulk = this.EquippedBulk
        member this.Price = this.Price

type ConsumableItem =
    { Id : string
      Name : string
      Description : string
      Level : int
      Rarity : Rarity
      Traits : Trait list
      NormalBulk : BulkValue
      EquippedBulk : BulkValue
      Price : Price option }
    interface IEquippableItem with
        member this.Id = this.Id
        member this.Name = this.Name
        member this.Description = this.Description
        member this.Level = this.Level
        member this.Rarity = this.Rarity
        member this.Traits = this.Traits
        member this.NormalBulk = this.NormalBulk
        member this.EquippedBulk = this.EquippedBulk
        member this.Price = this.Price

type ItemStack = { Item : IItem ; Amount : int }

type StorageInventory =
    { StorageItem : PhysicalStorageItem
      Items : ItemStack list }

type Item =
    | HeldItem of HeldItem
    | WeaponItem of WeaponItem
    | GearItem of GearItem
    | WornItem of WornItem
    | ArmorItem of ArmorItem
    | ShieldItem of ShieldItem
    | ConsumableItem of ConsumableItem
    | PhysicalStorageItem of PhysicalStorageItem
    | ReadableItem of ReadableItem

module Items =
    let playerCoinPurse =
        { Id = "player-coins"
          Name = "Coins"
          Description = ""
          Level = 0
          Rarity = Common
          Traits = []
          NormalBulk = BulkValue.Negligible
          EquippedBulk = BulkValue.Negligible
          ReduceBulk = None
          MaxBulkCapacity = None
          Price = None }

    let playerCoinPurseInventory =
        { StorageItem = playerCoinPurse
          Items = [] }

    let backpack =
        { Id = "backpack"
          Name = "Backpack"
          Description = ""
          Level = 0
          Rarity = Common
          Traits = []
          NormalBulk = BulkValue.Light 1
          EquippedBulk = BulkValue.Negligible
          ReduceBulk = Some (Bulk 2)
          MaxBulkCapacity = Some (Bulk 4)
          Price = Some (sp 1) }

    let playerBackPackInventory = { StorageItem = backpack ; Items = [] }

    let storageInventoryTotalBulk (inventory : StorageInventory) = inventory.StorageItem.ReduceBulk

    let addToInventory (item : IItem) inventory =
        let storageMax =
            inventory.StorageItem.MaxBulkCapacity |> Option.map (fun bulk -> bulk.ToInt ())

        let currentStoredBulk =
            inventory.Items
            |> List.map (fun i -> (i.Item.NormalBulk.ToInt ()) * i.Amount)
            |> List.sum

        let itemBulk = item.NormalBulk.ToInt ()

        let canAcceptItemBulk =
            storageMax
            |> Option.map (fun finiteMax -> itemBulk + currentStoredBulk <= finiteMax)
            |> Option.defaultValue true

        let existingItemStack =
            inventory.Items |> List.tryFindIndex (fun i -> i.Item = item)

        match canAcceptItemBulk, existingItemStack with
        | false, _ -> Error (InventoryError ExceedsBulk)
        | true, Some index ->
            let newItems =
                inventory.Items
                |> List.mapi (fun i stack ->
                    if i = index then
                        { stack with Amount = stack.Amount + 1 }
                    else
                        stack)

            Ok { inventory with Items = newItems }
        | true, None -> Ok { inventory with Items = { Item = item ; Amount = 1 } :: inventory.Items }

    let removeFromInventoryAt index inventory =
        { inventory with Items = inventory.Items |> List.removeAt index }
