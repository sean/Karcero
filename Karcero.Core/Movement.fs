module Karcero.Core.Movement

let terrainMvmtModifier t =
    match t with
    | Normal -> 0<ft>
    | Difficult -> 5<ft>
    | GreaterDifficult -> 10<ft>

let private additionalMvmtFromDiagonals numDiag =
    match numDiag with
    | n when n % 2 = 0 -> 5<ft>
    | _ -> 0<ft>

let rec private addMovement acc numDiag m =
    let movementCost = 5<ft>

    match m with
    | Movement (Straight, terrain, xs) ->
        let cost = movementCost + terrainMvmtModifier terrain

        addMovement (acc + cost) numDiag xs
    | Movement (Diagonal, terrain, xs) ->
        let newDiag = numDiag + 1

        let cost =
            movementCost + additionalMvmtFromDiagonals newDiag + terrainMvmtModifier terrain

        addMovement (acc + cost) newDiag xs
    | Start -> acc

let calculateMovement soFarMoved numDiagonalMoves measurement =
    addMovement soFarMoved numDiagonalMoves measurement

let movementToActions (speed : int<ft>) (movement : int<ft>) : int<actions> =
    (float movement / float speed)
    |> ceil
    |> int
    |> LanguagePrimitives.Int32WithMeasure

let start = Start

let add direction terrain measurement =
    match direction with
    | Straight -> Movement (Straight, terrain, measurement)
    | Diagonal -> Movement (Diagonal, terrain, measurement)
