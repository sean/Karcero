/// <summary>
/// Functions and utilities that are C#-friendly versions of the F# Core functions.
/// </summary>
module Karcero.Core.Bindings

open Dice
open Actors

let Round n = round n

let SystemConfiguration = systemConfiguration

let D4 = D4
let D6 = D6
let D8 = D8
let D12 = D12
let D20 = D20
let Roll (die : Die) = roll die

let Acrobatics = acrobatics
let Arcane = arcane
let Athletics = athletics
let Crafting = crafting
let Deception = deception
let Diplomacy = diplomacy
let Medicine = medicine
let Nature = nature
let Occultism = occultism
let Performance = performance
let Religion = religion
let World = world
let Stealth = stealth
let Survival = survival
let Thievery = thievery
let Intimidation = intimidation

let SkillToAbility skill = skillToAbility skill

let ProficiencyBonus num = Bonus.CreateProficiencyBonus num
let CircumstanceBonus num = Bonus.CreateCircumstanceBonus num
let StatusBonus num = Bonus.CreateStatusBonus num
let ItemBonus num = Bonus.CreateItemBonus num

let UntypedPenalty num = Penalty.CreateUntypedPenalty num
let CircumstancePenalty num = Penalty.CreateCircumstancePenalty num
let StatusPenalty num = Penalty.CreateStatusPenalty num
let ItemPenalty num = Penalty.CreateItemPenalty num

type PlayerCharacter() =
    let mutable self = newPc

    member _.GetAbilityModFromAbility ability =
        pcGetAbilityScoreFromAbility self ability

    member _.RollPerception (bonuses, penalties) =
        pcRollPerception (Array.toList bonuses) (Array.toList penalties) self

    member _.RollCheck (ability, abilityMod, bonuses, penalties) =
        pcRollCheck (Array.toList bonuses) (Array.toList penalties) abilityMod ability

    member _.RollSkillCheck (skill, bonuses, penalties) =
        pcRollSkillCheck skill (Array.toList bonuses) (Array.toList penalties) self
