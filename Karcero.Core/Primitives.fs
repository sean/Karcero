namespace Karcero.Core

[<Measure>]
type ft

[<Measure>]
type dmg

[<Measure>]
type actions

[<Measure>]
type rounds

type SuccessDegree =
    | CriticalFailure
    | Failure
    | Success
    | CriticalSuccess

/// Errors when trying to equip armor to a character
type ArmorEquipError =
    /// Strength too low to equip armor
    | StrTooLow
    /// Proficiency not high enough to equip armor
    | ProfTooLow

type InventoryError =
    /// Tried to add something too bulky to inventory
    | ExceedsBulk

type PartyError =
    /// Party is full and can't add another character to it
    | NoSlotsRemaining
    /// Somehow you attempted to add a character that already is in the party?
    | AlreadyInParty

// FUTURE: Should figure how errors should categorized
type KarceroError =
    | ArmorEquipError of ArmorEquipError
    | InventoryError of InventoryError
    | PartyError of PartyError

type ExplorationMode =
    | Free
    | TurnBased

type Mode =
    | Encounter
    | Exploration of ExplorationMode
    | Downtime

type DarknessKind =
    | NormalDarkness
    | MagicalDarkness

type LightLevel =
    | BrightLight
    | DimLight
    | Darkness of DarknessKind

type EscalationStage =
    | Start
    | Dangerous
    | Deadly
    | Chaos

type LevelSet = int Set

type Trait = string

type IProvidesTraits =
    abstract member ProvidesTraits : Trait list

type ITraited =
    abstract member Traits : Trait list

type IBaseSource =
    abstract member Id : string
    abstract member Name : string
    abstract member Description : string

type Condition =
    { Id : string
      Name : string
      Description : string
      Value : int option }
    interface IBaseSource with
        member this.Id = this.Id
        member this.Name = this.Name
        member this.Description = this.Description

type ArmorQuality =
    | Basic
    | Expert
    | Master
    | Legendary

type WeaponQuality =
    | Basic
    | Expert
    | Master
    | Legendary

type Proficiency =
    | Untrained
    | Trained
    | Expert
    | Master
    | Legendary

type Rarity =
    | Common
    | Uncommon
    | Rare
    | Unique

type ComplexBulkAmount = { Bulk : int ; Light : int }

[<CustomEquality ; CustomComparison>]
type BulkValue =
    | Bulk of int
    | Light of int
    | Negligible
    member x.ToInt () =
        match x with
        | Bulk n -> n * 10
        | Light n -> n
        | Negligible -> 0

    member x._Equals y =
        match x, y with
        | Negligible, Negligible -> true
        | Negligible, _ -> false
        | _, Negligible -> false
        | Bulk x, Bulk y -> y = x
        | Bulk x, Light y -> y = (x * 10)
        | Light x, Bulk y -> x = (y * 10)
        | Light x, Light y -> x = y

    static member (+) (x : BulkValue, y : BulkValue) = x.ToInt () + y.ToInt ()
    static member (-) (x : BulkValue, y : BulkValue) = x.ToInt () - y.ToInt ()

    override x.Equals obj =
        match obj with
        | :? BulkValue as y -> x._Equals y
        | _ -> false

    override x.GetHashCode () = hash (x.ToString ())

    interface System.IEquatable<BulkValue> with
        member x.Equals y = x._Equals y

    interface System.IComparable with
        member x.CompareTo obj =
            match obj with
            | :? BulkValue as y ->
                match x, y with
                | Bulk x, Bulk y -> compare x y
                | Bulk x, Light y -> compare (x * 10) y
                | Bulk x, Negligible -> compare x 0
                | Light x, Bulk y -> compare x (y * 10)
                | Light x, Light y -> compare x y
                | Light x, Negligible -> compare x 0
                | Negligible, Bulk x -> compare x 0
                | Negligible, Light x -> compare x 0
                | Negligible, Negligible -> 0
            | _ -> invalidArg "obj" "cannot compare values of different types"

type BulkAmount =
    | ComplexBulk of ComplexBulkAmount
    | SimpleBulk of BulkValue

type IBaseUnleveledData =
    inherit ITraited
    abstract member Id : string
    abstract member Name : string

type IBaseLeveledData =
    inherit IBaseUnleveledData
    abstract member Level : int

type IRareItem =
    abstract member Rarity : Rarity

type EncounterDifficulty =
    | Trivial
    | Low
    | Moderate
    | Severe
    | Deadly

type Disposition =
    | Hostile
    | Unfriendly
    | Indifferent
    | Friendly
    | Helpful

/// Takes a *positive* number
type Bonus =
    | InnateBonus of int
    | ProficiencyBonus of int
    | CircumstanceBonus of int
    | StatusBonus of int
    | ItemBonus of int

    member this.ToInt () =
        match this with
        | InnateBonus n -> n
        | ProficiencyBonus n -> n
        | CircumstanceBonus n -> n
        | StatusBonus n -> n
        | ItemBonus n -> n

    static member CreateInnateBonus num =
        if num < 0 then
            invalidArg "num" "Number must be greater than or equal to zero"

        InnateBonus num

    static member CreateProficiencyBonus num =
        if num < 0 then
            invalidArg "num" "Number must be greater than or equal to zero"

        ProficiencyBonus num

    static member CreateCircumstanceBonus num =
        if num < 0 then
            invalidArg "num" "Number must be greater than or equal to zero"

        CircumstanceBonus num

    static member CreateStatusBonus num =
        if num < 0 then
            invalidArg "num" "Number must be greater than or equal to zero"

        StatusBonus num

    static member CreateItemBonus num =
        if num < 0 then
            invalidArg "num" "Number must be greater than or equal to zero"

        ItemBonus num

/// Takes a *negative* number.
type Penalty =
    | CircumstancePenalty of int
    | StatusPenalty of int
    | ItemPenalty of int
    | UntypedPenalty of int

    member this.ToInt () =
        match this with
        | CircumstancePenalty n -> n
        | StatusPenalty n -> n
        | ItemPenalty n -> n
        | UntypedPenalty n -> n

    static member CreateUntypedPenalty num =
        if num > 0 then
            invalidArg "num" "Number must be less than or equal to zero"

        UntypedPenalty num

    static member CreateCircumstancePenalty num =
        if num > 0 then
            invalidArg "num" "Number must be less than or equal to zero"

        CircumstancePenalty num

    static member CreateStatusPenalty num =
        if num > 0 then
            invalidArg "num" "Number must be less than or equal to zero"

        StatusPenalty num

    static member CreateItemPenalty num =
        if num > 0 then
            invalidArg "num" "Number must be less than or equal to zero"

        ItemBonus num

type CheckResult =
    { Dc : int
      ResultValue : int
      Bonuses : Bonus list
      Penalties : Penalty list
      Result : SuccessDegree }

type ActionCost =
    | OneAction
    | TwoActions
    | ThreeActions
    | FourActions // From quickened condition
    | FreeAction
    | Reaction
    | Passive
    | NotPossible

type Trigger =
    | YourTurnBegins
    | TargetedByMeleeAttack
    | TargetedByRangedAttack

type MeleeWeaponGroups =
    | Axe
    | Brawling
    | Club
    | Dart
    | Flail
    | Hammer
    | Knife
    | Pick
    | Polearm
    | Shield
    | Spear
    | Sword

type WeaponReload =
    | Thrown
    | Reload0
    | Reload1
    | Reload3
    | Reload1Minute

type Ability =
    | Strength
    | Constitution
    | Dexterity
    | Intelligence
    | Wisdom
    | Charisma

type DcAdjustment =
    | IncrediblyEasy
    | VeryEasy
    | Easy
    | NoAdjustment
    | Hard
    | VeryHard
    | IncrediblyHard

type Sense =
    | StandardVision
    | LowLightVision
    | DarkVision
    | GreaterDarkvision
    | Hearing
    | Echolocation
    | Scent
    | Tremorsense
    | Lifesense
    | MotionSense
    | SeeInvisibility
    | SpiritSense
    | TremorSense
    | WaveSense
    | Other of name : string

type MovementType =
    | Speed
    | Swim
    | Fly
    | Climb
    | Burrow
    | Other of name : string

type StrengthSkill =
    | Athletics
    | StrengthKnowledge of name : string
    | OtherStrengthSkill of name : string

type DexteritySkill =
    | Acrobatics
    | Stealth
    | Thievery
    | StealthKnowledge of name : string
    | OtherDexteritySkill of name : string

type ConstitutionSkill =
    | ConstitutionKnowledge of name : string
    | OtherConstitutionSkill of name : string

type IntelligenceSkill =
    | Arcane
    | Crafting
    | IntelligenceKnowledge of name : string
    | Occultism
    | World
    | OtherIntelligenceSkill of name : string

type WisdomSkill =
    | Medicine
    | Nature
    | Religion
    | Survival
    | WisdomKnowledge of name : string
    | OtherWisdomSkill of name : string

type CharismaSkill =
    | Deception
    | Diplomacy
    | Intimidation
    | Performance
    | CharismaKnowledge of name : string
    | OtherCharismaSkill of name : string

type Skill =
    | StrengthSkill of StrengthSkill
    | DexteritySkill of DexteritySkill
    | ConstitutionSkill of ConstitutionSkill
    | IntelligenceSkill of IntelligenceSkill
    | WisdomSkill of WisdomSkill
    | CharismaSkill of CharismaSkill

type WeaponSkills =
    | SimpleWeapons
    | MartialWeapons
    | AdvancedWeapons
    | Unarmed

type ArmorSkills =
    | Unarmored
    | LightArmor
    | MediumArmor
    | HeavyArmor

type MartialSkills =
    { WeaponSkills : WeaponSkills
      ArmorSkills : ArmorSkills }

type SavingThrows =
    | Fortitude
    | Will
    | Reflex

type Currency =
    | Copper
    | Silver
    | Gold

type Price = { Currency : Currency ; Amount : int }

type Element =
    | Air
    | Earth
    | Fire
    | Metal
    | Water

type NonMeleeWeaponGroups =
    | Bomb
    | Bow
    | Sling

type WeaponGroups =
    { MeleeWeaponGroups : MeleeWeaponGroups
      NonMeleeWeaponGroups : NonMeleeWeaponGroups }

type SpellCategory =
    | Spell
    | Focus
    | Ritual

type CharacterSizeOrientation =
    | Tall
    | Long

type CharacterSizeCategory =
    | Tiny
    | Small
    | Medium
    | Large
    | Huge
    | Gargantuan

type CharacterSize =
    { Category : CharacterSizeCategory
      Orientation : CharacterSizeOrientation }

    member this.ToBulk () =
        match this.Category with
        | Tiny -> Bulk 1
        | Small -> Bulk 3
        | Medium -> Bulk 6
        | Large -> Bulk 12
        | Huge -> Bulk 24
        | Gargantuan -> Bulk 48

type FrequencyEvery =
    | EveryMinute
    | Every10Minutes
    | EveryHour
    | Every24Hours
    | EveryDay
    | EveryWeek
    | EveryYear

type FrequencyNumTimes =
    | Number of int
    | MaxNumber of int

type Frequency =
    { NumTimes : FrequencyNumTimes
      Every : FrequencyEvery }

type PhysicalDamageTypes =
    | Bleed
    | Bludgeoning
    | Piercing
    | Slashing

type EnergyDamageTypes =
    | Acid
    | Cold
    | Electricity
    | Fire
    | Force
    | Water
    | Negative
    | Positive
    | Sonic

type DamageSubTypes =
    | Persistent
    | Splash

type DamageType =
    | PhysicalDamage of PhysicalDamageTypes
    | EnergyDamage of EnergyDamageTypes
    | MentalDamage
    | PoisonDamage
    | BleedDamage
    | PrecisionDamage

type DamageSelectors =
    | AllPhysicalExcept of except : Set<PhysicalDamageTypes>
    | AllPhysical
    | AllMagicExcept of except : Set<DamageType>
    | AllMagic

type DamageEntry =
    { DamageType : DamageType
      Amount : int }

type DealtDamage = DamageEntry list

type TerrainRoughness =
    | Normal
    | Difficult
    | GreaterDifficult

type Direction =
    | Straight
    | Diagonal

type Movements =
    | Movement of Direction * TerrainRoughness * Movements
    | Start

type Action =
    { Id : string
      Name : string
      Traits : Trait list }
    interface IBaseUnleveledData with
        member this.Id = this.Id
        member this.Name = this.Name
        member this.Traits = this.Traits

type Cover =
    | LesserCover
    | StandardCover
    | GreaterCover

[<AutoOpen>]
module Primitives =
    // Almost all rounding is rounding down.
    let round (n : float) = (floor >> int) n

    let roundUp (n : float) = (ceil >> int) n

    let upgradeCover cover =
        match cover with
        | LesserCover -> StandardCover
        | StandardCover -> GreaterCover
        | GreaterCover -> GreaterCover

    let defaultMode = Exploration Free
    let combatMode = Encounter

    let hasMagicalTrait (traits : Trait list) = List.contains "magical" traits

    let dcModifier difficulty =
        match difficulty with
        | Casual -> -1
        | Tactical -> 0
        | Punishing -> 1
        | Mythical -> 2
        | Miserable -> 3

    let rarityToAdjustment rarity =
        match rarity with
        | Uncommon -> Hard
        | Rare -> VeryHard
        | Unique -> IncrediblyHard
        | _ -> NoAdjustment

    let dcAdjustment adjustment =
        match adjustment with
        | IncrediblyEasy -> -10
        | VeryEasy -> -5
        | Easy -> -2
        | NoAdjustment -> 0
        | Hard -> 2
        | VeryHard -> 5
        | IncrediblyHard -> 10

    let dc adjustment num = num + dcAdjustment adjustment

    let increaseEscalationStage escalationStage =
        match escalationStage with
        | EscalationStage.Start -> EscalationStage.Dangerous
        | EscalationStage.Dangerous -> EscalationStage.Deadly
        | EscalationStage.Deadly -> EscalationStage.Chaos
        | EscalationStage.Chaos -> EscalationStage.Chaos

    let escalationValueFromStage escalationStage =
        match escalationStage with
        | EscalationStage.Start -> 0
        | EscalationStage.Dangerous -> 1
        | EscalationStage.Deadly -> 2
        | EscalationStage.Chaos -> 3

    let increaseDisposition disposition =
        match disposition with
        | Hostile -> Unfriendly
        | Unfriendly -> Indifferent
        | Indifferent -> Friendly
        | Friendly -> Helpful
        | Helpful -> Helpful

    let decreaseDisposition disposition =
        match disposition with
        | Hostile -> Hostile
        | Unfriendly -> Hostile
        | Indifferent -> Unfriendly
        | Friendly -> Indifferent
        | Helpful -> Friendly

    let intToBulkAmount i : BulkAmount =
        let bulk = i / 10
        let light = i % 10

        match i <= 0, bulk, light with
        | true, _, _ -> SimpleBulk Negligible
        | _, 0, 0 -> SimpleBulk Negligible
        | _, x, 0 -> SimpleBulk (Bulk x)
        | _, 0, x -> SimpleBulk (Light x)
        | _, x, y -> ComplexBulk { Bulk = x ; Light = y }

    let proficiencyBonus p =
        match p with
        | Untrained -> 0
        | Trained -> 2
        | Expert -> 4
        | Master -> 6
        | Legendary -> 8

    let increaseProficiency p =
        match p with
        | Untrained -> Trained
        | Trained -> Expert
        | Expert -> Master
        | Master -> Legendary
        | Legendary -> Legendary

    let abilityModFromInt num = int (floor ((float num - 10.) / 2.))

    let coinsToBulkValue numCoins =
        match numCoins / 1000 with
        | 0 -> Negligible
        | x -> (Bulk x)

    let acrobatics = DexteritySkill Acrobatics
    let arcane = IntelligenceSkill Arcane
    let athletics = StrengthSkill Athletics
    let crafting = IntelligenceSkill Crafting
    let deception = CharismaSkill Deception
    let diplomacy = CharismaSkill Diplomacy
    let medicine = WisdomSkill Medicine
    let nature = WisdomSkill Nature
    let occultism = IntelligenceSkill Occultism
    let performance = CharismaSkill Performance
    let religion = WisdomSkill Religion
    let world = IntelligenceSkill World
    let stealth = DexteritySkill Stealth
    let survival = WisdomSkill Survival
    let thievery = DexteritySkill Thievery
    let intimidation = CharismaSkill Intimidation

    let gp n = { Currency = Gold ; Amount = n }
    let sp n = { Currency = Silver ; Amount = n }
    let cp n = { Currency = Copper ; Amount = n }

    let sizeToReach size =
        match size.Category, size.Orientation with
        | Tiny, _ -> 0<ft>
        | Small, _ -> 5<ft>
        | Medium, _ -> 5<ft>
        | Large, Tall -> 10<ft>
        | Large, Long -> 5<ft>
        | Huge, Tall -> 15<ft>
        | Huge, Long -> 10<ft>
        | Gargantuan, Tall -> 20<ft>
        | Gargantuan, Long -> 15<ft>

    let sizeToSpace size =
        match size.Category with
        | Tiny -> 0<ft>
        | Small -> 5<ft>
        | Medium -> 5<ft>
        | Large -> 10<ft>
        | Huge -> 15<ft>
        | Gargantuan -> 20<ft>

    let skillToAbility skill =
        match skill with
        | StrengthSkill _ -> Strength
        | DexteritySkill _ -> Dexterity
        | ConstitutionSkill _ -> Constitution
        | IntelligenceSkill _ -> Intelligence
        | WisdomSkill _ -> Wisdom
        | CharismaSkill _ -> Charisma

    let coreSkills =
        Set.ofList
            [ // Strength
              StrengthSkill Athletics
              // Dexterity
              DexteritySkill Acrobatics
              DexteritySkill Stealth
              DexteritySkill Thievery
              // Intelligence
              IntelligenceSkill Arcane
              IntelligenceSkill Crafting
              IntelligenceSkill Occultism
              IntelligenceSkill World
              // Wisdom
              WisdomSkill Medicine
              WisdomSkill Nature
              WisdomSkill Religion
              WisdomSkill Survival
              // Charisma
              CharismaSkill Deception
              CharismaSkill Intimidation
              CharismaSkill Performance ]
