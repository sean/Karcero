using Godot;

namespace Karcero.Game.Components
{
    public class Position
    {
        public Vector2i X, Y;
    }
}