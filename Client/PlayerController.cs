namespace Karcero.Client;

using Godot;

public partial class PlayerController : Node2D
{
    private bool _isMoving;
    private RemoteTransform2D _cameraRemote;
    private Sprite2D _characterSprite;
    private AnimationPlayer _animationPlayer;
    private Tween _tween;
    private SignalsController _signalsController;

    public override void _Ready()
    {
        _animationPlayer = GetNode<AnimationPlayer>("AnimationPlayer");
        _characterSprite = GetNode<Sprite2D>("SpriteContainer/CharacterSprite");
        _cameraRemote = GetNode<RemoteTransform2D>("CameraRemote");
        _signalsController = GetNode<SignalsController>("/root/SignalsController");

        _signalsController.Connect("PauseGame", new Callable(this, nameof(OnPause)));
        _signalsController.Connect("ResumeGame", new Callable(this, nameof(OnResume)));

        StartIdleAnimation();
    }

    private void OnPause()
    {
        _animationPlayer.Stop();
    }

    private void OnResume()
    {
        _animationPlayer.Play();
    }

    private void StartIdleAnimation()
    {
        _animationPlayer.Play("Idle");
    }

    private void OnAfterMove()
    {
        _isMoving = false;
    }

    public void ConnectCamera(Camera2D camera)
    {
        var cameraPath = camera.GetPath();
        _cameraRemote.RemotePath = cameraPath;
    }

    private void ChangeLookDirection(Vector2 positionToLookAt)
    {
        var positionToLookAtLocal = ToLocal(positionToLookAt);
        if (positionToLookAtLocal.x != 0)
        {
            _characterSprite.FlipH = positionToLookAtLocal.x < 0;
        }
    }

    public void MovePlayerToGlobal(Vector2 newPosition)
    {
        if (!_isMoving)
        {
            if (_tween != null)
                _tween.Kill();
            ChangeLookDirection(newPosition);
            _isMoving = true;
            _tween = CreateTween();
            _tween.TweenProperty(this, new NodePath("global_position"), newPosition, 0.5)
                .SetEase(Tween.EaseType.Out)
                .SetTrans(Tween.TransitionType.Quint);

            _tween.TweenCallback(new Callable(this, nameof(OnAfterMove)));
        }
    }
}