namespace Karcero.Client;

using Godot;
using RelEcs;
using Debug;

public partial class Main : Node
{
    private readonly World _world = new();
    private readonly SystemGroup _initSystems = new();
    private readonly SystemGroup _runSystems = new();
    private readonly SystemGroup _cleanupSystems = new();

    private SignalsController _signalsController;
    private CanvasLayer _mainMenuContainerNode;
    private PackedScene _mainMenuScene;

    private void ClearMainMenuContainer()
    {
        foreach (var child in _mainMenuContainerNode.GetChildren())
        {
            child.QueueFree();
        }
    }

    private void LoadMainMenu()
    {
        if (_mainMenuContainerNode.GetChildCount() > 0)
            ClearMainMenuContainer();

        _mainMenuContainerNode.AddChild(_mainMenuScene.Instantiate<MainMenu>());
    }

    private void RemoveMainMenu()
    {
        ClearMainMenuContainer();
    }

    private void CloseGame()
    {
        GetTree().Quit();
    }

    public override void _Ready()
    {
        _mainMenuContainerNode = GetNode<CanvasLayer>("MainMenuContainer");
        _signalsController = GetNode<SignalsController>("/root/SignalsController");
        _mainMenuScene = ResourceLoader.Load<PackedScene>("res://Client/MainMenu.tscn");

        // Attach to signals
        _signalsController.Connect("StartingGameSession", new Callable(this, nameof(RemoveMainMenu)));
        _signalsController.Connect("ExitGame", new Callable(this, nameof(CloseGame)));
        _signalsController.Connect("GameSessionUnloaded", new Callable(this, nameof(LoadMainMenu)));

        Logger.Info("Ready!");
        _initSystems.Run(_world);
    }

    public override void _Process(double delta)
    {
        _runSystems.Run(_world);
        _world.Tick();
    }

    public override void _ExitTree()
    {
        _cleanupSystems.Run(_world);
    }
}