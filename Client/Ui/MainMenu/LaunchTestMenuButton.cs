namespace Karcero.Client.Ui.MainMenu;

using Godot;

public partial class LaunchTestMenuButton : Button
{
    public override void _Ready()
    {
        var signalsController = GetNode<SignalsController>("/root/SignalsController");
        var launchTestLevel = Callable.From(() =>
        {
            signalsController.EmitSignal("LaunchLevel", "res://Client/Levels/TestLevel.tscn");
        });
        Connect("pressed", launchTestLevel);
    }
}