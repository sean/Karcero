namespace Karcero.Client.Ui.MainMenu;

using Godot;

public partial class ExitGameButton : Button
{
    public override void _Ready()
    {
        var signalsController = GetNode<SignalsController>("/root/SignalsController");
        Connect("pressed", Callable.From(() => { signalsController.EmitSignal("ExitGame"); }));
    }
}