namespace Karcero.Client;

using Godot;

public partial class SignalsController : Node
{
    [Signal]
    public delegate void LaunchLevelEventHandler(string level);

    [Signal]
    public delegate void LevelLaunchedEventHandler();

    [Signal]
    public delegate void CreateNewGameEventHandler();

    [Signal]
    public delegate void LoadGameEventHandler();

    [Signal]
    public delegate void GameLoadedEventHandler();

    [Signal]
    public delegate void SaveGameEventHandler();

    [Signal]
    public delegate void GameSavedEventHandler();

    [Signal]
    public delegate void QuitToDesktopEventHandler();

    [Signal]
    public delegate void QuitToMenuEventHandler();

    [Signal]
    public delegate void ToggleOpenInGameMenuEventHandler();

    [Signal]
    public delegate void InGameMenuOpenedEventHandler();

    [Signal]
    public delegate void InGameMenuClosedEventHandler();

    [Signal]
    public delegate void ExitGameEventHandler();

    [Signal]
    public delegate void StartingGameSessionEventHandler();

    [Signal]
    public delegate void UnloadGameSessionEventHandler();

    [Signal]
    public delegate void GameSessionUnloadedEventHandler();

    [Signal]
    public delegate void GamePausedEventHandler();

    [Signal]
    public delegate void GameResumedEventHandler();

    [Signal]
    public delegate void PauseGameEventHandler();

    [Signal]
    public delegate void ResumeGameEventHandler();
}