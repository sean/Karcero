namespace Karcero.Client.Levels;

using Godot;

public partial class TestLevel : Node2D, IKarceroLevel
{
    private PlayerController _playerController;
    private TileMap _tileMap;
    private Sprite2D _gridCursor;
    private Node2D _playerContainer;

    public void Pause()
    {
        SetProcess(false);
        SetPhysicsProcess(false);
        SetProcessInput(false);
    }

    public void Resume()
    {
        SetProcess(true);
        SetPhysicsProcess(true);
        SetProcessInput(true);
    }

    public TileMap LevelTileMap()
    {
        return _tileMap;
    }

    public Vector2 PlaceCharacterAt(string hint)
    {
        var playerSpawnMarkerPosition = GetNode<Marker2D>("TileMap/PlayerSpawnMarker").Position;
        return playerSpawnMarkerPosition;
    }

    public void PlaceCharacter(Vector2 characterSpawnPoint, Camera2D mainCamera, Sprite2D gridCursor)
    {
        _gridCursor = gridCursor;
        var playerControllerScene = ResourceLoader.Load<PackedScene>("res://Client/PlayerController.tscn");
        var playerController = playerControllerScene.Instantiate<PlayerController>();
        _playerController = playerController;
        _playerContainer.AddChild(playerController);
        var globalCoords = GetPlayerMoveCoords(characterSpawnPoint);
        playerController.GlobalPosition = globalCoords;
        playerController.ConnectCamera(mainCamera);
    }

    private Vector2 TileMapLocalToCellGlobal(Vector2 localCoords)
    {
        var currentHoveredCoords = _tileMap.LocalToMap(localCoords);
        var cellWorldPositionLocal = _tileMap.MapToLocal(currentHoveredCoords);
        var cellWorldPositionGlobal = _tileMap.ToGlobal(cellWorldPositionLocal);
        return cellWorldPositionGlobal;
    }

    private void MoveGridCursor()
    {
        _gridCursor.GlobalPosition = TileMapLocalToCellGlobal(_tileMap.GetLocalMousePosition());
    }

    private Vector2 GetPlayerMoveCoords(Vector2 localCoords)
    {
        var cellWorldPositionGlobal = TileMapLocalToCellGlobal(localCoords);
        return cellWorldPositionGlobal;
    }

    public void MovePlayer(Vector2 localCoords)
    {
        var cellWorldPositionGlobal = GetPlayerMoveCoords(localCoords);
        _playerController.MovePlayerToGlobal(cellWorldPositionGlobal);
    }

    public override void _Ready()
    {
        _tileMap = GetNode<TileMap>("TileMap");
        _playerContainer = _tileMap.GetNode<Node2D>("PlayerContainer");
    }

    public override void _Process(double delta)
    {
        if (_gridCursor != null)
            MoveGridCursor();
    }
}