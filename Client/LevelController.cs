namespace Karcero.Client;

using Godot;

public partial class LevelController : Node
{
    private Node2D _levelContainer;
    private Camera2D _mainCamera;
    private Sprite2D _gridCursor;
    private TileMap _levelTileMap;
    private ColorRect _screenFader;
    private Tween _screenFaderTween;
    private IKarceroLevel _level;

    public void PauseLevel()
    {
        if (_level != null)
        {
            _level.Pause();
        }
    }

    public void ResumeLevel()
    {
        if (_level != null)
        {
            _level.Resume();
        }
    }

    private void OnAfterFadeIn(Callable callback)
    {
        _mainCamera.PositionSmoothingEnabled = true;
        _screenFader.MouseFilter = Control.MouseFilterEnum.Pass;
        _level.Resume();
        _gridCursor.Visible = true;
        callback.Call();
    }

    private void ClearLevel(Callable callback)
    {
        _level = null;
        _levelTileMap = null;
        foreach (var child in _levelContainer.GetChildren())
        {
            child.QueueFree();
        }

        callback.Call();
    }

    public void LoadPackedLevelScene(string path, Callable callback)
    {
        ResetFade();
        var levelResource = ResourceLoader.Load<PackedScene>(path).Instantiate();
        _level = (IKarceroLevel) levelResource;
        var levelNode2d = (Node2D) levelResource;
        _levelContainer.AddChild(levelResource);
        var placeCharacterAt = _level.PlaceCharacterAt("test");
        _levelTileMap = _level.LevelTileMap();
        _mainCamera.GlobalPosition = levelNode2d.ToGlobal(placeCharacterAt);
        _gridCursor.GlobalPosition = levelNode2d.ToGlobal(placeCharacterAt);
        _mainCamera.PositionSmoothingEnabled = false;
        _level.PlaceCharacter(placeCharacterAt, _mainCamera, _gridCursor);
        FadeIn(callback);
    }

    public void CleanupForExit(Callable callback)
    {
        FadeOut(Callable.From(() => { ClearLevel(callback); }));
    }

    private void ResetFade()
    {
        if (_screenFaderTween != null)
            _screenFaderTween.Kill();
        _gridCursor.Visible = false;
        _screenFaderTween = CreateTween();
        _screenFader.MouseFilter = Control.MouseFilterEnum.Stop;
        _screenFader.Color = Color.Color8(0, 0, 0);
    }

    private void FadeIn(Callable callback)
    {
        if (_screenFaderTween != null)
            _screenFaderTween.Kill();
        _screenFaderTween = CreateTween();
        _level.Pause();
        _screenFader.Color = Color.Color8(0, 0, 0);
        _screenFaderTween.TweenProperty(_screenFader, "color", Color.Color8(0, 0, 0, 0), 1);
        _screenFaderTween.TweenCallback(Callable.From(() => { OnAfterFadeIn(callback); }));
    }

    private void FadeOut(Callable callback)
    {
        if (_screenFaderTween != null)
            _screenFaderTween.Kill();
        _gridCursor.Visible = false;
        _screenFaderTween = CreateTween();
        _level.Pause();
        _screenFader.Color = Color.Color8(0, 0, 0, 0);
        _screenFaderTween.TweenProperty(_screenFader, "color", Color.Color8(0, 0, 0), 1);
        _screenFaderTween.TweenCallback(callback);
    }

    public override void _Ready()
    {
        _levelContainer = GetNode<Node2D>("LevelContainer");
        _mainCamera = GetNode<Camera2D>("MainCamera");
        _gridCursor = GetNode<Sprite2D>("GridCursor");
        _screenFader = GetNode<ColorRect>("ScreenFadeLayer/ScreenFader");
    }

    public void MoveCharacter()
    {
        _level.MovePlayer(_levelTileMap.GetLocalMousePosition());
    }
}