using Godot;

namespace Karcero.Client;

public interface IKarceroLevel
{
    Vector2 PlaceCharacterAt(string hint);

    void PlaceCharacter(Vector2 characterSpawnPoint, Camera2D mainCamera, Sprite2D gridCursor);

    TileMap LevelTileMap();

    void Pause();

    void Resume();

    void MovePlayer(Vector2 localMousePosition);
}