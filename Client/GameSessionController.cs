namespace Karcero.Client;

using Godot;

public partial class GameSessionController : Node
{
    // TODO: Move as much of this logic to Karcero.Core
    private bool _levelIsTransitioning;
    private bool _levelLoaded;
    private SignalsController _signalsController;
    private LevelController _levelController;
    private Node2D _gameSessionContainer;
    private CanvasLayer _hudLayer;
    private bool _isGamePaused;
    private ColorRect _gamePauseLabelContainer;

    private bool CanPerformInGameActions()
    {
        return CanPause() && !_isGamePaused;
    }

    private bool CanPause()
    {
        return _levelLoaded && !_levelIsTransitioning;
    }

    private void PauseGame()
    {
        _levelController.PauseLevel();
        _isGamePaused = true;
        _gamePauseLabelContainer.Visible = true;
        _signalsController.EmitSignal("GamePaused");
    }

    private void ResumeGame()
    {
        _levelController.ResumeLevel();
        _isGamePaused = false;
        _gamePauseLabelContainer.Visible = false;
        _signalsController.EmitSignal("GameResumed");
    }

    private void MoveCharacter()
    {
        if (CanPerformInGameActions())
        {
            _levelController.MoveCharacter();
        }
    }

    private void TogglePause()
    {
        if (CanPause())
        {
            _signalsController.EmitSignal(_isGamePaused ? "ResumeGame" : "PauseGame");
        }
    }

    // TODO: Implement
    private void SaveGame()
    {
        _signalsController.EmitSignal("GameSaved");
    }

    // TODO: Implement
    private void LoadGame()
    {
        _signalsController.EmitSignal("GameLoaded");
    }

    private void TryCleanupForExit()
    {
        if (!_isGamePaused)
        {
            CleanupForExit();
        }
    }

    public void CleanupForExit()
    {
        _levelIsTransitioning = true;
        _levelController.CleanupForExit(Callable.From(() =>
        {
            _signalsController.EmitSignal("GameSessionUnloaded");
            _gameSessionContainer.Visible = false;
            _hudLayer.Visible = false;
            _levelIsTransitioning = false;
            _levelLoaded = false;
        }));
    }

    public void StartSessionWithScene(string path)
    {
        _signalsController.EmitSignal("StartingGameSession");
        _gameSessionContainer.Visible = true;
        _hudLayer.Visible = true;
        _levelIsTransitioning = true;
        _levelController.LoadPackedLevelScene(path, Callable.From(() =>
        {
            _levelIsTransitioning = false;
            _levelLoaded = true;
        }));
    }

    public override void _UnhandledInput(InputEvent @event)
    {
        if (@event.IsActionReleased("Move", true))
        {
            MoveCharacter();
        }

        if (@event.IsActionReleased("ESC", true))
        {
            _signalsController.EmitSignal("QuitToMenu");
        }

        if (@event.IsActionReleased("ToggleInGamePause", true) && CanPause())
        {
            TogglePause();
        }
    }

    public override void _Ready()
    {
        _signalsController = GetNode<SignalsController>("/root/SignalsController");
        _gameSessionContainer = GetNode<Node2D>("GameSessionContainer");
        _levelController = _gameSessionContainer.GetNode<LevelController>("LevelController");
        _hudLayer = _gameSessionContainer.GetNode<CanvasLayer>("HudLayer");
        _gamePauseLabelContainer = _gameSessionContainer.GetNode<ColorRect>("HudLayer/PauseLabelContainer");

        _signalsController.Connect("ResumeGame", new Callable(this, nameof(ResumeGame)));
        _signalsController.Connect("PauseGame", new Callable(this, nameof(PauseGame)));
        _signalsController.Connect("QuitToMenu", new Callable(this, nameof(TryCleanupForExit)));
        _signalsController.Connect("UnloadGameSession", new Callable(this, nameof(CleanupForExit)));
        _signalsController.Connect("LaunchLevel", new Callable(this, nameof(StartSessionWithScene)));
        _signalsController.Connect("SaveGame", new Callable(this, nameof(SaveGame)));
        _signalsController.Connect("LoadGame", new Callable(this, nameof(LoadGame)));

        _gameSessionContainer.Visible = false;
        _hudLayer.Visible = false;
    }
}