format:
    find ./ -type f -name "*.fs" -not -path "*obj*" | xargs dotnet fantomas

@test:
    dotnet watch test